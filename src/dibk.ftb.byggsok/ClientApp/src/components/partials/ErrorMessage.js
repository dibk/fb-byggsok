// Dependencies
import React, { Component } from 'react';
import { connect } from 'react-redux';

// DIBK Design
import { Header, Paper } from 'dibk-design';

// Stylesheets
import style from 'components/partials/ErrorMessage.module.scss';

class ErrorMessage extends Component {
  render() {
      return (<div>
          <Header content={this.props.error.title} />
          <Paper>
              <p>{this.props.error.description}</p>
          </Paper>
        </div>);
  }
}

export default connect(null, null)(ErrorMessage);
