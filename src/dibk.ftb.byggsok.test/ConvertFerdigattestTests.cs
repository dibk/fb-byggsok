﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using dibk.ftb.byggsok.Services;
using dibk.ftb.byggsok.Util;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using no.dibk.byggsok;
using no.kxml.skjema.dibk.ferdigattestV2;
using Xunit;

namespace dibk.ftb.byggsok.test
{
    public class ConvertFerdigattestTests
    {
        private readonly ILogger<SerializeUtil> _loggerSerializeUtil = new Logger<SerializeUtil>(new NullLoggerFactory());
        private readonly ILogger<ConvertFerdigattest> _logger = new Logger<ConvertFerdigattest>(new NullLoggerFactory());
        [Fact(DisplayName = "GetTiltakshaverPartId OK")]
        public void FerdigatestsGetTiltakshaverIdtOkTest()

        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            var byggsok = new SerializeUtil(_loggerSerializeUtil).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);

            var tiltakshaverId = ConvertFerdigattest.GetTiltakshaverPartId(byggsok);
            var partType = ConvertFerdigattest.GetPart(tiltakshaverId, byggsok);

            tiltakshaverId.Should().Be("ID_1515414803085_2"); //privatperson

        }
        [Fact(DisplayName = "GetBySPart OK")]
        public void FerdigatestsGetPartOkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            var byggsok = new SerializeUtil(_loggerSerializeUtil).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);

            var partType = ConvertFerdigattest.GetPart("ID_1515414803085_2", byggsok); //privatperson

            partType.Should().NotBeNull();

        }
        [Fact(DisplayName = "GetFtbEnkelAdresseType OK")]
        public void FerdigatestsGetAdresseTypeOkTest()
        {


            var enkelAdresseType = new enkelAdresseType()
            {
                adresselinje = new[]
                {
                    new adresselinjeType(){Item = "Osloveien 40 J",}
                },
                landskodeSpecified = true,
                landskode = 0,
                postnummer = "1440",
                poststed = "Drøbak"

            };

            var bsAdresseType = ConvertFerdigattest.ConvertByggsonEnkelAdresseToFtb(enkelAdresseType);

            bsAdresseType.Should().NotBeNull();

        }

        [Fact(DisplayName = "GetPartTypeForForetak OK")]
        public void FerdigatestsGetPartTypeForForetakOkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            var byggsok = new SerializeUtil(_loggerSerializeUtil).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var tiltakshaver = ConvertFerdigattest.GetPart("ID_1515414803085_11", byggsok);

            var partType = ConvertFerdigattest.GetPartTypeForForetak(tiltakshaver.Item);

            partType.Should().NotBeNull();

        }
        [Fact(DisplayName = "GetPartTypeForPrivatperson OK")]
        public void FerdigatestsGetPartTypeForPrivatpersonOkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            var byggsok = new SerializeUtil(_loggerSerializeUtil).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var tiltakshaver = ConvertFerdigattest.GetPart("ID_1515414803085_2", byggsok);

            var partType = ConvertFerdigattest.GetPartTypeForPrivatperson(tiltakshaver.Item);

            partType.epost.Should().Be("hildegl@gmail.com");

        }
        [Fact(DisplayName = "GetPartTypeForOffentligMyndighetn OK", Skip = "det finnes ikke noe eksample data")]
        public void FerdigatestsGetPartTypeForOffentligMyndighetnOkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            var byggsok = new SerializeUtil(_loggerSerializeUtil).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var tiltakshaver = ConvertFerdigattest.GetPart("ID_1515414806666_2", byggsok);

            var partType = ConvertFerdigattest.GetPartTypeForOffentligMyndighetn(tiltakshaver.Item);

            partType.epost.Should().Be("hildegl@gmail.com");
        }
        [Fact(DisplayName = "GetAnsvarligSokerFtbParsType OK")]
        public void FerdigatestsGetAnsvarligSokerParsTypenOkTest()
        {

            //string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            //var byggsok = new SerializeUtil(_loggerSerializeUtil).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            byggesak byggsok = new byggesak()
            {
                gjennomforingsplan = new gjennomforingsplan()
                {
                    ansvarsfordeling = new[]
                    {
                        new ansvarsfordeling()
                        {
                            ansvarligsoker = new AnsvarligForArbeidType()
                            {
                                annenkompetanseSpecified = true,
                                ansvarlig = new partrefType()
                                {
                                    @ref = "ID_1515414806666_2"
                                }
                            }
                        },
                    }
                }
            };
            byggsok.part = new[]
            {
                new partType()
                {
                    id = "ID_1515414806666_2",
                    Item = new partTypePrivatperson()
                    {
                        navn = "unitTest"
                    }


                },

            };


            var ansvarligSoker = ConvertFerdigattest.GetAnsvarligSokerParsType(byggsok);

            ansvarligSoker.navn.Should().Be("unitTest");
        }

        [Fact(DisplayName = "GetTiltakshaverParsType OK")]
        public void FerdigatestsGetTiltakshaverParsTypeOkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            //var byggsok = Util.SerializeUtil.DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            byggesak byggsok = new byggesak()
            {
                rolle = new byggesakRolle[]
                {
                    new byggesakRolle()
                    {
                        id = "ID_1515414806666_1",
                        typeSpecified = true,
                        type = byggesakRolleType.tiltakshaver,
                        referansetilpart = new partrefType()
                        {
                            @ref = "ID_1515414806666_2"
                        }
                    },
                }
            };
            byggsok.part = new[]
            {
                new partType()
                {
                    id = "ID_1515414806666_2",
                    Item = new partTypePrivatperson()
                    {
                        navn = "TiltakshaverPars"
                    }


                },

            };


            var ansvarligSoker = ConvertFerdigattest.GetTiltakshaverParsType(byggsok);

            ansvarligSoker.navn.Should().Be("TiltakshaverPars");
        }
        [Fact(DisplayName = "eiendomByggested OK")]
        public void FerdigatestseiendomByggestedOkTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);

            var ferdigatestsV2Type =new ConvertFerdigattest(_logger).Convert(byggsok);
            ferdigatestsV2Type.eiendomByggested.Should().NotBeNull();
        }
        [Fact(DisplayName = "tiltakshaver OK")]
        public void FerdigatestsTiltakshaverdOkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var ferdigatestsV2Type =new ConvertFerdigattest(_logger).Convert(byggsok);
            ferdigatestsV2Type.tiltakshaver.Should().NotBeNull();
        }
        [Fact(DisplayName = "ansvarligSoeker OK")]
        public void FerdigatestsAnsvarligSoekerOkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var ferdigatestsV2Type =new ConvertFerdigattest(_logger).Convert(byggsok);
            ferdigatestsV2Type.ansvarligSoeker.Should().NotBeNull();
        }
        [Fact(DisplayName = "Kommplet - ByggesakDebug_5 OK")]
        public void FerdigatestsByggesakDebug_5OkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_5.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var ferdigatestsV2Type =new ConvertFerdigattest(_logger).Convert(byggsok);
            ferdigatestsV2Type.ansvarligSoeker.Should().NotBeNull();
            ferdigatestsV2Type.tiltakshaver.Should().NotBeNull();
            ferdigatestsV2Type.eiendomByggested.Should().NotBeNull();
        }
        [Fact(DisplayName = "Kommplet - ByggesakDebug_7 OK")]
        public void FerdigatestsByggesakDebug_7OkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_7.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var ferdigatestsV2Type =new ConvertFerdigattest(_logger).Convert(byggsok);
            ferdigatestsV2Type.ansvarligSoeker.Should().NotBeNull();
            ferdigatestsV2Type.tiltakshaver.Should().NotBeNull();
            ferdigatestsV2Type.eiendomByggested.Should().NotBeNull();
        }
        [Fact(DisplayName = "Kommplet - ByggesakDebug_9 OK")]
        public void FerdigatestsByggesakDebug_9OkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_9.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var ferdigatestsV2Type =new ConvertFerdigattest(_logger).Convert(byggsok);
            ferdigatestsV2Type.ansvarligSoeker.Should().NotBeNull();
            ferdigatestsV2Type.tiltakshaver.Should().NotBeNull();
            ferdigatestsV2Type.eiendomByggested.Should().NotBeNull();
        }
    }
}
