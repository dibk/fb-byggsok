﻿using dibk.ftb.byggsok.Util;
using Microsoft.Extensions.Logging;
using no.dibk.byggsok;
using no.kxml.skjema.dibk.midlertidigbrukstillatelseV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dibk.ftb.byggsok.Services
{
    public class ConvertMidlertidigbrukstillatelse
    {
        private ILogger _logger;

        public ConvertMidlertidigbrukstillatelse(ILogger logger)
        {
            _logger = logger;
        }

        public no.kxml.skjema.dibk.midlertidigbrukstillatelseV2.MidlertidigBrukstillatelseType Convert(byggesak byggsok)
        {
            var resultForm = new MidlertidigBrukstillatelseType();

            resultForm.fraSluttbrukersystem = "Byggsøk";
            resultForm.prosjektnavn = "";
            resultForm.harTilstrekkeligSikkerhetSpecified = false;

            //resultForm.hovedinnsendingsnummer = byggsok.soknadid;
            //resultForm.versjon = "";

            if (byggsok == null)
                return resultForm;

            // saksaar and sakssekvensnummer
            AddkommunensSaksnummerAar(byggsok, resultForm);

            //TODO historikk på tidligere søknader om MB
            //resultForm.delsoeknad
            List<DelsoeknadMidlertidigBrukstillatelseType> midlertidiDelsoknade = null;
            if (byggsok.byggesaksprosess != null)
            {
                if (byggsok.byggesaksprosess.midlertidigbrukstillatelse != null)
                {
                    foreach (var bsMB in byggsok.byggesaksprosess.midlertidigbrukstillatelse)
                    {
                        foreach (var bsDel in bsMB.delsoknad)
                        {
                            var delsoknad = new DelsoeknadMidlertidigBrukstillatelseType();
                            delsoknad.delAvTiltaket = bsDel.beskrivelse;
                            if (bsDel.tidligeremidlertidigdato != null)
                            {
                                delsoknad.tillatelsesdato = bsDel.tidligeremidlertidigdato;
                                delsoknad.tillatelsesdatoSpecified = true;
                            }

                            midlertidiDelsoknade = midlertidiDelsoknade ?? new List<DelsoeknadMidlertidigBrukstillatelseType>();
                            midlertidiDelsoknade.Add(delsoknad);
                        }
                    }
                    resultForm.delsoeknad = midlertidiDelsoknade?.ToArray();
                }
            }

            //Byggeeiendom
            try
            {
                if (!Helpers.ObjectIsNullOrEmpty(byggsok.eiendom))
                {
                    List<EiendomType> eiendombyggested = new List<EiendomType>();
                    foreach (var bseiendom in byggsok.eiendom)
                    {
                        if (bseiendom.typeSpecified && bseiendom.type == no.dibk.byggsok.eiendomType.byggeeiendom)
                        {
                            var eiendomType = new EiendomType();
                            if (bseiendom.eiendomsidentifikasjon != null)
                            {
                                eiendomType.eiendomsidentifikasjon = new MatrikkelnummerType();
                                eiendomType.eiendomsidentifikasjon.kommunenummer = bseiendom.eiendomsidentifikasjon.kommunenummer;
                                eiendomType.eiendomsidentifikasjon.gaardsnummer = bseiendom.eiendomsidentifikasjon.gardsnummer;
                                eiendomType.eiendomsidentifikasjon.bruksnummer = bseiendom.eiendomsidentifikasjon.bruksnummer;
                                eiendomType.eiendomsidentifikasjon.festenummer = bseiendom.eiendomsidentifikasjon.festenummer;
                                eiendomType.eiendomsidentifikasjon.seksjonsnummer = bseiendom.eiendomsidentifikasjon.seksjonsnummer;
                            }

                            var adresse = bseiendom.adresse.First();
                            if (adresse != null)
                            {
                                //Tar bare med første adresse fra byggsøk
                                eiendomType.adresse = new EiendommensAdresseType();
                                var bsAdresseType = adresse.adresselinje?.FirstOrDefault();
                                if (!string.IsNullOrEmpty(bsAdresseType?.Item?.ToString()))
                                    eiendomType.adresse.adresselinje1 = bsAdresseType.Item?.ToString();

                                eiendomType.adresse.postnr = adresse.postnummer;
                                eiendomType.adresse.poststed = adresse.poststed;
                            }
                            eiendombyggested.Add(eiendomType);
                        }
                    }
                    resultForm.eiendomByggested = eiendombyggested.ToArray();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Feil ved konvertering av eiendomsidentifikasjon for MidlertidigBrukstillatelse");
            }

            //Tiltakshaver
            try
            {
                resultForm.tiltakshaver = GetTiltakshaverParsType(byggsok);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Feil ved konvertering av tiltakshaver for MidlertidigBrukstillatelse");
            }

            //Ansvarlig søker
            try
            {
                resultForm.ansvarligSoeker = GetAnsvarligSokerParsType(byggsok);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Feil ved konvertering av ansvarlig søker for MidlertidigBrukstillatelse");
            }

            return resultForm;
        }

        /// <summary>
        /// Konver saksid To saksnummerType
        /// </summary>
        /// <param name="byggsok"></param>
        /// <param name="resultForm"></param>
        public void AddkommunensSaksnummerAar(byggesak byggsok, MidlertidigBrukstillatelseType resultForm)
        {
            if (byggsok.identifier != null)
            {
                var municipalityInfo = new ByggsokUtilities(_logger).GetMunicipalityYaerAndNumber(byggsok);
                if (municipalityInfo != null && municipalityInfo.Any())
                {
                    resultForm.kommunensSaksnummer = new SaksnummerType();
                    resultForm.kommunensSaksnummer.saksaar = municipalityInfo["saksaar"].ToString();
                    resultForm.kommunensSaksnummer.sakssekvensnummer = municipalityInfo["sakssekvensnummer"].ToString();
                }
            }
        }

        public static PartType GetAnsvarligSokerParsType(byggesak byggsok)
        {
            PartType ftbPartType = null;
            if (!Helpers.ObjectIsNullOrEmpty(byggsok.gjennomforingsplan?.ansvarsfordeling))
            {
                AnsvarligForArbeidType ansvarligForArbeid = null;
                if (byggsok.gjennomforingsplan.ansvarsfordeling.Any(a => a.ansvarligsoker != null))
                    ansvarligForArbeid = byggsok.gjennomforingsplan?.ansvarsfordeling.First(a => a.ansvarligsoker != null).ansvarligsoker;

                partType ansvarligsokerBsPartType = null;

                if (!Helpers.ObjectIsNullOrEmpty(ansvarligForArbeid?.ansvarlig?.@ref))
                    ansvarligsokerBsPartType = GetPart(ansvarligForArbeid.ansvarlig?.@ref, byggsok);

                if (ansvarligsokerBsPartType != null)
                    ftbPartType = GetPartsTypeFromPart(ansvarligsokerBsPartType.Item);
            }

            if (ftbPartType == null)
            {
                var isAnsvarligsoke = byggsok.rolle?.Any(r => r.type == byggesakRolleType.ansvarligsøker);
                if (isAnsvarligsoke.GetValueOrDefault(false))
                {
                    var tilatkshaverId = byggsok.rolle.First(r => r.type == byggesakRolleType.ansvarligsøker).referansetilpart?.@ref;
                    var part = GetPart(tilatkshaverId, byggsok);
                    if (part != null)
                    {
                        ftbPartType = GetPartsTypeFromPart(part.Item);
                    }
                }
            }
            return ftbPartType;
        }

        public static PartType GetTiltakshaverParsType(byggesak byggsok)
        {
            var isTilatkshaver = byggsok.rolle?.Any(r => r.type == byggesakRolleType.tiltakshaver);
            if (isTilatkshaver.GetValueOrDefault(false))
            {
                var tilatkshaverId = byggsok.rolle?.First(r => r.type == byggesakRolleType.tiltakshaver).referansetilpart?.@ref;
                var part = GetPart(tilatkshaverId, byggsok);
                if (part != null)
                {
                    return GetPartsTypeFromPart(part.Item);
                }
            }
            return null;
        }

        public static partType GetPart(string id, byggesak byggsok)
        {
            partType part = null;
            if (!string.IsNullOrEmpty(id))
            {
                var partlist = byggsok.part.ToList().Where(p => p.id == id).ToArray();
                if (partlist.Any()) part = partlist.First();
            }
            return part;
        }

        public static PartType GetPartsTypeFromPart(object item)
        {
            var typeName = item.GetType().Name;

            PartType partType = null;
            switch (typeName)
            {
                case "foretak":
                case "partTypeForetak":
                    partType = GetPartTypeForForetak(item);
                    break;

                case "offentligmyndighet":
                case "partTypeOffentligmyndighet":
                    partType = GetPartTypeForOffentligMyndighetn(item);
                    break;

                case "organisasjon":
                case "partTypeOrganisasjon":
                    partType = GetPartTypeForOrganisasjon(item);
                    break;

                case "privatperson":
                case "partTypePrivatperson":
                    partType = GetPartTypeForPrivatperson(item);
                    break;
            }

            return partType;
        }

        public static PartType GetPartTypeForForetak(object item)
        {
            try
            {
                var partTypeForetak = (partTypeForetak)item;

                var partType = new PartType
                {
                    organisasjonsnummer = partTypeForetak.foretaksnummer,
                    navn = partTypeForetak.navn,
                    telefonnummer = partTypeForetak.telefonnummer,
                    epost = partTypeForetak.epostadresse,
                    partstype = new KodeType()
                    {
                        kodeverdi = "foretak",
                        kodebeskrivelse = "foretak"
                    },
                };

                partType.adresse = ConvertByggsonEnkelAdresseToFtb(partTypeForetak.adresse);
                ConvertByggsokKontaktInformasjonToFtb(partTypeForetak.kontaktinformasjon, partType);

                return partType;
            }
            catch
            {
                return null;
            }
        }

        public static PartType GetPartTypeForOrganisasjon(object item)
        {
            try
            {
                var partTypeOrganisasjon = (partTypeOrganisasjon)item;

                var partType = new PartType
                {
                    organisasjonsnummer = partTypeOrganisasjon.organisasjonsnummer,
                    navn = partTypeOrganisasjon.navn,
                    partstype = new KodeType()
                    {
                        kodeverdi = "Organisasjon",
                        kodebeskrivelse = "Organisasjon"
                    },
                };

                partType.adresse = ConvertByggsonEnkelAdresseToFtb(partTypeOrganisasjon.adresse);
                ConvertByggsokKontaktInformasjonToFtb(partTypeOrganisasjon.kontaktinformasjon, partType);

                return partType;
            }
            catch
            {
                return null;
            }
        }

        public static PartType GetPartTypeForPrivatperson(object item)
        {
            try
            {
                var partTypePrivatperson = (partTypePrivatperson)item;

                var partType = new PartType
                {
                    partstype = new KodeType()
                    {
                        kodeverdi = "Privatperson",
                        kodebeskrivelse = "Privatperson"
                    },
                    navn = partTypePrivatperson.navn
                };

                partType.adresse = ConvertByggsonEnkelAdresseToFtb(partTypePrivatperson.adresse);
                ConvertByggsokKontaktInformasjonToFtb(partTypePrivatperson.kontaktinformasjon, partType);

                return partType;
            }
            catch
            {
                return null;
            }
        }

        public static PartType GetPartTypeForOffentligMyndighetn(object item)
        {
            try
            {
                var partTypeOffentligmyndighet = (partTypeOffentligmyndighet)item;

                var partType = new PartType
                {
                    partstype = new KodeType()
                    {
                        kodeverdi = "Offentlig myndighet",
                        kodebeskrivelse = "Offentlig myndighet"
                    },
                    navn = partTypeOffentligmyndighet.navn
                };

                ConvertByggsokKontaktInformasjonToFtb(partTypeOffentligmyndighet.kontaktinformasjon, partType);

                return partType;
            }
            catch
            {
                return null;
            }
        }

        public static EnkelAdresseType ConvertByggsonEnkelAdresseToFtb(enkelAdresseType adresseType)
        {
            if (adresseType == null)
                return null;

            var ftbAdresseType = new EnkelAdresseType()
            {
                poststed = adresseType.poststed,
                postnr = adresseType.postnummer,
            };
            if (adresseType.landskodeSpecified)
                ftbAdresseType.landkode = adresseType.landskode.ToString();

            var bsAdresseType = adresseType.adresselinje?.FirstOrDefault();
            if (!string.IsNullOrEmpty(bsAdresseType?.Item?.ToString()))
                ftbAdresseType.adresselinje1 = bsAdresseType.Item?.ToString();

            return ftbAdresseType;
        }

        public static void ConvertByggsokKontaktInformasjonToFtb(kontaktinformasjonType kontaktinformasjon, PartType ftbPartType)
        {
            if (kontaktinformasjon != null)
            {
                ftbPartType.mobilnummer = ftbPartType.mobilnummer ?? kontaktinformasjon.mobiltelefonnummer;
                ftbPartType.epost = ftbPartType.epost ?? kontaktinformasjon.epostadresse;
                ftbPartType.kontaktperson = ftbPartType.kontaktperson ?? kontaktinformasjon.personnavn;
                ftbPartType.navn = ftbPartType.navn ?? kontaktinformasjon.personnavn;
                ftbPartType.telefonnummer = ftbPartType.telefonnummer ?? kontaktinformasjon.telefonnummer;
                if (Helpers.ObjectIsNullOrEmpty(ftbPartType.adresse))
                    ftbPartType.adresse = ConvertByggsonEnkelAdresseToFtb(kontaktinformasjon.adresse);
            }
        }
    }
}