using Microsoft.Extensions.Logging;
using no.dibk.byggsok;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace dibk.ftb.byggsok.Util
{
    public class SerializeUtil
    {
        private ILogger _logger;

        public SerializeUtil(ILogger logger)
        {
            _logger = logger;
        }

        private static List<string> _xmlErrorList = new List<string>();
        private static List<string> _xmlWarningList;
        private static int _validationErrorCountLimit = 100;

        public T DeserializeByggesakFromString<T>(string objectData)
        {
            var classObject = (T)DeserializeByggesakFromString(objectData, typeof(T));
            try
            {
                if (classObject is byggesak)
                {
                    var byggesakObject = classObject as byggesak;
                    if (Helpers.ObjectIsNullOrEmpty(byggesakObject.saksid?.First().eier))
                    {
                        var saksid = GetSaksidFromXml(objectData);
                        byggesakObject.saksid = new[] { new byggesakSaksid() { eier = saksid } };

                        //https://stackoverflow.com/a/899636
                        classObject = (T)Convert.ChangeType(byggesakObject, typeof(T));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Problem adding 'saksid' to byggesak class");
            }
            return classObject;
        }

        private object DeserializeByggesakFromString(string objectData, Type type)
        {
            var serializer = new XmlSerializer(type);
            serializer.UnknownElement += new XmlElementEventHandler(Serializer_UnknownElement);
            serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);
            serializer.UnreferencedObject += new UnreferencedObjectEventHandler(Serializer_UnreferencedObject);

            object result = null;

            TextReader reader = null;
            try
            {
                string standardizedXmlString = !objectData.Contains("&amp;") ? objectData.Replace("&", "&amp;") : objectData;
                reader = new StringReader(standardizedXmlString);
                result = serializer.Deserialize(reader);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Can't Deserialize xml {xmlErrorMessage}");
                //Send an error message to get a better log of the missing node / value in byggesak xml
                ValidateXmlWithBygssok15Schema(objectData, e.InnerException?.Message);
                throw;
            }
            finally
            {
                reader?.Close();
            }

            return result;
        }

        public static string Serialize(object form)
        {
            var serializer = new System.Xml.Serialization.XmlSerializer(form.GetType());
            var stringWriter = new Utf8StringWriter();
            serializer.Serialize(stringWriter, form);
            return stringWriter.ToString();
        }

        // To debug xml
        private static void Serializer_UnreferencedObject(object sender, UnreferencedObjectEventArgs e)
        {
            var objectBeingDeserialized = e.UnreferencedObject.ToString();
            var unreferencedId = e.UnreferencedId;
            var unreferencedObject = e.UnreferencedObject;
        }

        private static void Serializer_UnknownElement(object sender, XmlElementEventArgs e)
        {
            var objectBeingDeserialized = e.ObjectBeingDeserialized.ToString();
            var elementName = e.Element.Name;
            var elementInnerXml = e.Element.InnerXml;
            var lineNumber = e.LineNumber;
            var linePosition = e.LinePosition;
        }

        private static void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            var name = e.Name;
            var objectBeingDeserialized = e.ObjectBeingDeserialized.ToString();
            var localName = e.LocalName;
            var namespaceURI = e.NamespaceURI;
            var text = e.Text;
        }

        private static void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            var attrName = e.Attr.Name;
            var attrInnerXml = e.Attr.InnerXml;
            var lineNumber = e.LineNumber;
            var linePosition = e.LinePosition;
            var objectBeingDeserialized = e.ExpectedAttributes;
        }

        public string GetSaksidFromXml(string byggsokXml)
        {
            //https://stackoverflow.com/a/16452514

            string saksid = null;
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(byggsokXml);
                XmlNodeList byggsokSaksidElement = doc.GetElementsByTagName("saksid");
                if (byggsokSaksidElement.Count < 1)
                {
                    // The tag could not be fond
                }
                else
                {
                    // The tag could be found!
                    var elementAtributes = byggsokSaksidElement[0].Attributes;
                    if (elementAtributes.Count > 0)
                    {
                        saksid = elementAtributes[0].InnerText;
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Problem geting 'saksid' from byggesak Xml ");
            }
            return saksid;
        }

        // XML validation
        public void ValidateXmlWithBygssok15Schema(string byggsokxml, string xmlErrorMessage)
        {
            try
            {
                XDocument doc = XDocument.Parse(byggsokxml);
                string rootLocalName = doc.Root.Name.LocalName;
                var text = '<' + rootLocalName + '>';
                if (rootLocalName != "byggesak")
                {
                    _logger.LogError("XML not supported {XmlFirstNode}", text);
                    return;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unsupported XMl string ");
                return;
            }

            XmlSchema schema = null;
            try
            {
                string xsdSchema = @"byggsok\byggsok15.xsd";
                using (var schemaReader = XmlReader.Create(xsdSchema))
                {
                    schema = XmlSchema.Read(schemaReader, ValidationCallBack);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Can't read byggsok15.xsd schema");
            }
            var schemas = new XmlSchemaSet();
            schemas.Add(schema);
            // setup
            var settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas = schemas;
            settings.ValidationFlags =
                XmlSchemaValidationFlags.ProcessIdentityConstraints |
                XmlSchemaValidationFlags.ReportValidationWarnings;
            settings.ValidationEventHandler += ValidationCallBack;
            try
            {
                using (var validationReader = XmlReader.Create(byggsokxml, settings))
                {
                    while (validationReader.Read())
                    {
                        if (_xmlErrorList.Count >= _validationErrorCountLimit)
                        {
                            _logger.LogError("Byggsøk Xml exceeds error limit ({xmlErrorCountLimit})", _validationErrorCountLimit);
                            break;
                        }
                    }
                }
                _logger.LogWarning("Byggsøk 1.5 xml validation Warnings : {@xmlWarnings}", _xmlWarningList);
                _logger.LogError("Byggsøk 1.5 xml validation Warnings : {@xmlErrors}", _xmlErrorList);
            }
            catch
            {
                // We don't log the exception because it may contain sensitive data
                _logger.LogError("Reading Xml for validation {xmlErrorMessage}", xmlErrorMessage);
            }
        }

        private static void ValidationCallBack(object sender, ValidationEventArgs args)
        {
            if (args.Severity == XmlSeverityType.Warning)
            {
                _xmlWarningList.Add(string.Concat("linje " + args.Exception.LineNumber + ", posisjon " + args.Exception.LinePosition + " " + args.Message, "XSD validering"));
            }
            else if (args.Severity == XmlSeverityType.Error)
            {
                _xmlErrorList.Add(string.Concat("linje " + args.Exception.LineNumber + ", posisjon " + args.Exception.LinePosition + " " + args.Message, "XSD validering"));
            }
        }

        public T DeserializeFromString<T>(string formSkjema)
        {
            StringReader strReader = null;
            XmlTextReader xmlReader = null;

            XDocument doc = XDocument.Parse(formSkjema);
            string rootLocalName = doc.Root.Name.LocalName;

            Object obj = null;
            try
            {
                var serializer = new XmlSerializer(typeof(T));
                strReader = new StringReader(formSkjema);
                xmlReader = new XmlTextReader(strReader);
                obj = serializer.Deserialize(xmlReader);
            }
            catch (Exception exp)
            {
                var message = exp.Message;
                //
            }
            finally
            {
                xmlReader?.Close();
                strReader?.Close();
            }
            return (T)Convert.ChangeType(obj, typeof(T));
        }
    }
}