import { UPDATE_SELECTED_NEXT_PROCESS_CATEGORY } from 'constants/types';

const initialState = "";

export default function(state = initialState, action) {
	switch(action.type) {
		case UPDATE_SELECTED_NEXT_PROCESS_CATEGORY:
			return action.payload;
		default:
			return state;
	}

}
