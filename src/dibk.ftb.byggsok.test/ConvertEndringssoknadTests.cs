﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using dibk.ftb.byggsok.Services;
using dibk.ftb.byggsok.Util;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Xunit;

namespace dibk.ftb.byggsok.test
{
    public class ConvertEndringssoknadTests
    {
        private readonly ILogger<ConvertEndringssoknad> _logger = new Logger<ConvertEndringssoknad>(new NullLoggerFactory());

        [Fact(DisplayName = "Kommplet - ByggesakDebug_5 OK")]
        public void EndringssoknadByggesakDebug_5OkTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_5.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var endringssoknadV2Type =new  ConvertEndringssoknad(_logger).Convert(byggsok);
            endringssoknadV2Type.ansvarligSoeker.Should().NotBeNull();
            endringssoknadV2Type.tiltakshaver.Should().NotBeNull();
            endringssoknadV2Type.eiendomByggested.Should().NotBeNull();
        }
        [Fact(DisplayName = "Kommplet - ByggesakDebug_7 OK")]
        public void EndringssoknadByggesakDebug_7OkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_7.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var endringssoknadV2Type = new ConvertEndringssoknad(_logger).Convert(byggsok);
            endringssoknadV2Type.ansvarligSoeker.Should().NotBeNull();
            endringssoknadV2Type.tiltakshaver.Should().NotBeNull();
            endringssoknadV2Type.eiendomByggested.Should().NotBeNull();
        }
        [Fact(DisplayName = "Kommplet - ByggesakDebug_9 OK")]
        public void EndringssoknadByggesakDebug_9OkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_9.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var endringssoknadV2Type = new ConvertEndringssoknad(_logger).Convert(byggsok);
            endringssoknadV2Type.ansvarligSoeker.Should().NotBeNull();
            endringssoknadV2Type.tiltakshaver.Should().NotBeNull();
            endringssoknadV2Type.eiendomByggested.Should().NotBeNull();
        }
        [Fact(DisplayName = "Kommplet - ByggesakDebug_hilde OK")]
        public void EndringssoknadByggesakDebug_hildeOkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var endringssoknadV2Type = new ConvertEndringssoknad(_logger).Convert(byggsok);
            endringssoknadV2Type.ansvarligSoeker.Should().NotBeNull();
            endringssoknadV2Type.tiltakshaver.Should().NotBeNull();
            endringssoknadV2Type.eiendomByggested.Should().NotBeNull();
        }
    }
}
