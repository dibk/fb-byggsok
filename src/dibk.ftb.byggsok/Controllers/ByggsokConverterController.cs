﻿using dibk.ftb.byggsok.Model.Api;
using dibk.ftb.byggsok.Services;
using dibk.ftb.byggsok.Util;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace dibk.ftb.byggsok.Controllers
{
    [ApiController]
    public class ByggsokConverterController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly EmailService _emailService;
        private readonly ByggsokToFtbService _byggsokToFtbService;
        private string _nesteProsess;

        public ByggsokConverterController(ILogger<ByggsokConverterController> logger, EmailService emailService, ByggsokToFtbService byggsokToFtbService)
        {
            _logger = logger;
            _emailService = emailService;
            _byggsokToFtbService = byggsokToFtbService;
        }

        // POST api/ConvertToFTB
        /// <summary>
        /// Konverter ByggSøk XML til Fellestjenester BYGG XML.
        /// </summary>
        /// <param name="request">inneholder byggsøk xml [xml], neste søknadstype [nesteProsess] og Søknad system navn [applicationSystem]</param>
        /// <returns>
        /// Liste med skjema som kan benyttes mot Altinn API - https://altinn.github.io/docs/api/rest/meldinger/sende-inn/#post-for-innsending-av-skjema-til-arbeidslisten-mellomlagring
        /// </returns>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/ConvertToFTB")]
        [HttpPost]
        [EnableCors("AllowAllCors")]
        public async Task<ActionResult<ConvertFormTaskResponse>> ConvertToFTB([FromBody] ByggsokRequest request)
        {
            ConvertFormTaskResponse response;
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            if (!ModelState.IsValid)
            {
                _logger.LogError("Invalid model");
                return BadRequest("Ugyldig modell");
            }
            var applicationSystemName = request?.applicationSystem;

            var followingProcesses = new[]
            {
                "IG",
                "MB",
                "FA",
                "ES"
            };

            if (!followingProcesses.Any(x => x == request?.nesteProsess))
            {
                _logger.LogError("invalid next proccess value '{nesteProsess}'", request?.nesteProsess);
                return BadRequest("Ugyldig neste søknadstype");
            }

            try
            {
                _nesteProsess = request?.nesteProsess;

                _logger.LogDebug("star Byggsok Konvertering, converting to ({nextProcess}), application system Name {applicationSystem} ", _nesteProsess, applicationSystemName);
                response = _byggsokToFtbService.ConvertToFTB(request);

                stopWatch.Stop();
                _logger.LogInformation("Konvert to ({nextProcess}) ServiceCode:{serviceCode}, ServiceEdition:{serviceEdition} used {elapsedTime} - søknadssystem ('{applicationSystem}')", _nesteProsess, response.ServiceCode, response.ServiceEdition, stopWatch.Elapsed, applicationSystemName);
                return Ok(response);
            }
            catch (Exception ex)
            {
                stopWatch.Stop();
                _logger.LogError(ex, "Error to convert ({nextProcess}) konvertering used {elapsedTime} - søknadssystem ('{applicationSystem}') ", _nesteProsess, stopWatch.Elapsed, applicationSystemName);
                return BadRequest(ex);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [EnableCors("Wizard")]
        [Route("api/SendEmail")]
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<ActionResult> SendEmail([FromBody] EpostRequest request)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("Invalid epost model");
                return BadRequest("Invalid model");
            }

            var epostTask = _emailService.SendAsync(request);
            if (epostTask == null)
            {
                return BadRequest("can't create email");
            }

            _logger.LogInformation("Email sent standardizeEpost '{standardizeEmail}' AltinnArkivreferanse: '{AltinnArkivreferanse}' ", Helpers.StandardizeEmailToLogg(request.email), request.altinArchiveReferenceNumber);
            await epostTask.ConfigureAwait(false);

            return Ok("Email sent");
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [EnableCors("Wizard")]
        [Route("api/getprojectAddress")]
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        public ActionResult GetProsjectAddresssFromGjenomforinsplan([FromBody] Form form)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("Invalid epost model");
                return BadRequest("Invalid model");
            }

            if (form.DataFormatId != "6146" || form.DataFormatVersion != "44096")
            {
                return BadRequest("wrong DataFormatId and Data DataFormatVersion");
            }

            no.kxml.skjema.dibk.gjennomforingsplanV6.GjennomfoeringsplanType gjennomfoeringsplanType;

            try
            {
                gjennomfoeringsplanType = new SerializeUtil(_logger).DeserializeFromString<no.kxml.skjema.dibk.gjennomforingsplanV6.GjennomfoeringsplanType>(form.FormData);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "can't Deserialize GjennomfoeringsplanType to get property Address");
                return BadRequest("kan ikke Deserialize GjennomfoeringsplanType for '555' å få Address");
            }
                        
            try
            {
                var adresselinje1 = gjennomfoeringsplanType.eiendomByggested?.First().adresse?.adresselinje1;

                return Ok(adresselinje1);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Can't get Address from GjennomfoeringsplanType");
                return BadRequest("Can't get Address from GjennomfoeringsplanType");
            }
        }
    }
}