using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using no.dibk.byggsok;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace dibk.ftb.byggsok.Util
{
    public class ByggsokUtilities
    {
        private ILogger _logger;

        public ByggsokUtilities(ILogger logger)
        {
            _logger = logger;
        }

        public partType GetbyggsokPartype(string id, byggesak byggsok)
        {
            partType part = null;
            if (!String.IsNullOrEmpty(id))
            {
                var partlist = byggsok.part.ToList().Where(p => p.id == id).ToArray();
                if (partlist.Any()) part = partlist.First();
            }
            return part;
        }

        public DateTime? GetDateTimeFromSignatureRef(string signaturerefId, byggesak byggsok)
        {
            DateTime? dateTime = null;

            if (!String.IsNullOrEmpty(signaturerefId))
            {
                var signaturerefTypes = GetByggesakSignatureFromRefId(signaturerefId, byggsok);
                if (signaturerefTypes != null)
                {
                    if (signaturerefTypes.signeddateSpecified)
                    {
                        var signaturDateTime = signaturerefTypes.sentdate;
                        dateTime = signaturDateTime;
                    }
                }
            }
            return dateTime;
        }

        public byggesakSignature GetByggesakSignatureFromRefId(string signaturerefId, byggesak byggsok)
        {
            byggesakSignature signaturerefTypes = null;
            if (byggsok.signature.Any(s => s.id == signaturerefId))
            {
                signaturerefTypes = byggsok.signature.First(s => s.id == signaturerefId);
            }
            return signaturerefTypes;
        }

        /// <summary>
        /// Get all samsvarserklering/utforelse from UtfAnsvarligForArbeidType Id
        /// </summary>
        /// <param name="samsvarserkleringid"></param>
        /// <param name="byggsok"></param>
        /// <returns></returns>
        public samsvarserkleringUtforelse[] GetSamsvarserkleringUtforelse(string samsvarserkleringid, byggesak byggsok)
        {
            List<samsvarserkleringUtforelse> samsvarserkleringUtforelses = new List<samsvarserkleringUtforelse>();

            if (String.IsNullOrEmpty(samsvarserkleringid) || Helpers.ObjectIsNullOrEmpty(byggsok))
                return samsvarserkleringUtforelses.ToArray();

            try
            {
                var samsvarserckaringUtf = byggsok.gjennomforingsplan?.samsvarserklering?.Where(sa => sa.utforelse != null).ToArray();
                if (samsvarserckaringUtf != null)
                {
                    foreach (samsvarserklering samsvarserklering in samsvarserckaringUtf)
                    {
                        var samsvarserkleringUtforelse = samsvarserklering.utforelse;
                        if (samsvarserkleringUtforelse.Any())
                        {
                            foreach (samsvarserkleringUtforelse utforelse in samsvarserkleringUtforelse)
                            {
                                if (utforelse.ansvarsfordeling.@ref == samsvarserkleringid)
                                {
                                    samsvarserkleringUtforelses.Add(utforelse);
                                }
                            }
                        }
                    }
                }
                return samsvarserkleringUtforelses.ToArray();
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get all /gjennomforingsplan/samsvarserklering/prosjektering from /gjennomforingsplan/ansvarsfordeling/ansvarligprosjektering.Id
        /// </summary>
        /// <param name="samsvarserkleringid"></param>
        /// <param name="byggsok"></param>
        /// <returns></returns>
        public samsvarserkleringProsjektering[] GetSamsvarserkleringProsjektering(string samsvarserkleringid, byggesak byggsok)
        {
            List<samsvarserkleringProsjektering> samsvarserkleringProsjekterings = new List<samsvarserkleringProsjektering>();

            if (String.IsNullOrEmpty(samsvarserkleringid) || Helpers.ObjectIsNullOrEmpty(byggsok))
                return samsvarserkleringProsjekterings.ToArray();

            try
            {
                var samsvarserckaringPro = byggsok.gjennomforingsplan?.samsvarserklering?.Where(sa => sa.prosjektering != null).ToArray();
                if (samsvarserckaringPro != null)
                {
                    foreach (samsvarserklering samsvarserklering in samsvarserckaringPro)
                    {
                        var samsvarserkleringProsjektering = samsvarserklering.prosjektering;
                        if (samsvarserkleringProsjektering.Any())
                        {
                            foreach (samsvarserkleringProsjektering prosjektering in samsvarserkleringProsjektering)
                            {
                                if (prosjektering.ansvarsfordeling.@ref == samsvarserkleringid)
                                {
                                    samsvarserkleringProsjekterings.Add(prosjektering);
                                }
                            }
                        }
                    }
                }
                return samsvarserkleringProsjekterings.ToArray();
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get all /gjennomforingsplan/samsvarserklering/prosjektering from /gjennomforingsplan/ansvarsfordeling/ansvarligprosjektering.Id
        /// </summary>
        /// <param name="samsvarserkleringid"></param>
        /// <param name="byggsok"></param>
        /// <returns></returns>
        public kontrollerkleringKontroll[] GetSamsvarserkleringKontroll(string samsvarserkleringid, byggesak byggsok)
        {
            List<kontrollerkleringKontroll> kontrollerkleringKontrolls = new List<kontrollerkleringKontroll>();

            if (String.IsNullOrEmpty(samsvarserkleringid) || Helpers.ObjectIsNullOrEmpty(byggsok))
                return kontrollerkleringKontrolls.ToArray();

            try
            {
                var gjennomforingsplanKontrollerklering = byggsok.gjennomforingsplan?.kontrollerklering;
                if (gjennomforingsplanKontrollerklering != null)
                {
                    foreach (kontrollerkleringKontroll kontroll in gjennomforingsplanKontrollerklering)
                    {
                        if (kontroll.ansvarsfordeling.@ref == samsvarserkleringid)
                        {
                            kontrollerkleringKontrolls.Add(kontroll);
                        }
                    }
                }
                return kontrollerkleringKontrolls.ToArray();
            }
            catch
            {
                return null;
            }
        }

        public UtfAnsvarligForArbeidType[] GetAnsvarligForArbeidUTF(byggesak byggsok)
        {
            UtfAnsvarligForArbeidType[] utfAnsvarligForArbeid = null;
            if (Helpers.ObjectIsNullOrEmpty(byggsok?.gjennomforingsplan?.ansvarsfordeling)) return utfAnsvarligForArbeid;

            if (byggsok.gjennomforingsplan.ansvarsfordeling.Any(an => an.ansvarligutforelse != null))
                utfAnsvarligForArbeid = byggsok.gjennomforingsplan?.ansvarsfordeling?.Where(an => an.ansvarligutforelse != null).SelectMany(a => a.ansvarligutforelse).ToArray();

            return utfAnsvarligForArbeid;
        }

        public string GetSoknadTypeFrombygsokSignatur(byggesakSignature signature)
        {
            var soknadtypeString = signature?.soknadtype;
            string[] soknadtipes;
            if (string.IsNullOrEmpty(soknadtypeString))
                return null;

            if (soknadtypeString.Contains(","))
            {
                soknadtipes = signature?.soknadtype.Split(",");
            }
            else
            {
                soknadtipes = new[] { soknadtypeString };
            }

            var soknadTypes = soknadtipes.Distinct().ToArray();

            string soknadType = null;
            if (soknadTypes.Contains("Ferdigattest"))
            {
                soknadType = "Ferdigattest";
            }
            else if (soknadTypes.Contains("midlertidigbrukstillatelse"))
            {
                soknadType = "midlertidigbrukstillatelse";
            }
            else if (soknadTypes.Contains("etttrinnssoknad"))
            {
                soknadType = "etttrinnssoknad";
            }
            else if (soknadTypes.Contains("igangsettingstillatelse"))
            {
                soknadType = "igangsettingstillatelse";
            }
            else if (soknadTypes.Contains("rammetillatelse"))
            {
                soknadType = "rammetillatelse";
            }

            return soknadType;
        }

        public ProAnsvarligForArbeidType[] GetAnsvarligForArbeidPRO(byggesak byggsok)
        {
            if (Helpers.ObjectIsNullOrEmpty(byggsok))
                return null;
            var ansvarligForArbeidTypes = byggsok.gjennomforingsplan?.ansvarsfordeling?.Where(an => an.ansvarligprosjektering != null).SelectMany(a => a.ansvarligprosjektering).ToArray();
            return ansvarligForArbeidTypes;
        }

        public KonAnsvarligForArbeidType[] GetAnsvarligForArbeidKON(byggesak byggsok)
        {
            if (Helpers.ObjectIsNullOrEmpty(byggsok))
                return null;

            var konAnsvarligForArbeidTypes = byggsok.gjennomforingsplan?.ansvarsfordeling?.Where(an => an.ansvarligkontroll != null).SelectMany(a => a.ansvarligkontroll).ToArray();
            return konAnsvarligForArbeidTypes;
        }

        public Dictionary<string, int> GetMunicipalityYaerAndNumber(byggesak byggsok)
        {
            Dictionary<string, int> municipalityDictionary = new Dictionary<string, int>();
            string byggskoSaksnummer = null;

            if (byggsok?.saksid != null && byggsok.saksid.Any())
            {
                var noko = byggsok.saksid;
                byggskoSaksnummer = byggsok.saksid.FirstOrDefault()?.eier;
            }

            if (string.IsNullOrEmpty(byggskoSaksnummer))
                return municipalityDictionary;

            string[] municipalityInfo = null;
            // Kommune saksId format 16/1234
            if (byggskoSaksnummer.Contains("/"))
            {
                municipalityInfo = Regex.Split(byggskoSaksnummer, "/");
            }
            // Kommune saksId format 20161234
            else if (byggskoSaksnummer.Length >= 8)
            {
                var saksaar = byggskoSaksnummer.Substring(0, 4);
                var sakssekvensnummer = byggskoSaksnummer.Substring(4);

                municipalityInfo = new[] { saksaar, sakssekvensnummer };
            }

            if (municipalityInfo?.Length == 2)
            {
                int municipalityYear = 0;
                int municipalityNummer;
                try
                {
                    if (Int32.TryParse(municipalityInfo[0], out municipalityYear) && Int32.TryParse(municipalityInfo[1], out municipalityNummer))
                    {
                        var fourDigitYear = CultureInfo.CurrentCulture.Calendar.ToFourDigitYear(municipalityYear);
                        if (fourDigitYear > 2000)
                        {
                            municipalityDictionary.Add("saksaar", fourDigitYear);
                            municipalityDictionary.Add("sakssekvensnummer", municipalityNummer);
                        }
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Can't convert date and municipality Nummer:{saksid}", byggskoSaksnummer);
                }
            }
            else
            {
                _logger.LogDebug("whrong 'saksid' format:{saksid}", byggskoSaksnummer);
            }
            return municipalityDictionary;
        }

        public partType GetAnsvarligSokerParsType(byggesak byggsok)
        {
            partType ansvarligsokerBsPartType = null;
            if (!Helpers.ObjectIsNullOrEmpty(byggsok.gjennomforingsplan?.ansvarsfordeling))
            {
                var ansvarsfordeling = byggsok.gjennomforingsplan?.ansvarsfordeling.First(a => a.ansvarligsoker != null);
                var ansvarligsoker = ansvarsfordeling?.ansvarligsoker;

                if (!string.IsNullOrEmpty(ansvarligsoker?.ansvarlig?.@ref))
                {
                    ansvarligsokerBsPartType = GetPartTypeById(ansvarligsoker.ansvarlig?.@ref, byggsok);
                }
            }

            if (ansvarligsokerBsPartType == null)
            {
                var isAnsvarligsoke = byggsok.rolle?.Any(r => r.type == byggesakRolleType.ansvarligsøker);
                if (isAnsvarligsoke.GetValueOrDefault(false))
                {
                    var tilatkshaverId = byggsok.rolle.First(r => r.type == byggesakRolleType.ansvarligsøker).referansetilpart?.@ref;
                    ansvarligsokerBsPartType = GetPartTypeById(tilatkshaverId, byggsok);
                }
                else
                {
                    ansvarligsokerBsPartType = new partType();
                }
            }
            return ansvarligsokerBsPartType;
        }

        /// <summary>
        /// Gat Part typre from reference Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="byggsok"></param>
        /// <returns></returns>
        public no.dibk.byggsok.partType GetPartTypeById(string id, byggesak byggsok)
        {
            partType part = null;
            if (!String.IsNullOrEmpty(id))
            {
                var partlist = byggsok.part.ToList().Where(p => p.id == id).ToArray();
                if (partlist.Any()) part = partlist.First();
            }
            return part;
        }

        public Dictionary<string, List<byggesakSignature>> GetbyggsakPartTypeSigatures(string partTypeId, byggesak byggesok)
        {
            List<byggesakSignature> byggesakSignatures = new List<byggesakSignature>();
            List<byggesakSignature> byggesakSignaturesbl = new List<byggesakSignature>();

            var samsvarserkleringsId = byggesok.gjennomforingsplan.samsvarserklering.SelectMany(sa => sa.utforelse?.Where(utf => utf.ansvarsfordeling.@ref == partTypeId)).ToArray();

            foreach (samsvarserkleringUtforelse samsvarserkleringUtforelse in samsvarserkleringsId)
            {
                var signatur = GetByggesakSignatureFromRefId(samsvarserkleringUtforelse.signatureref.@ref, byggesok);
                if (signatur.blockedreasonSpecified)
                {
                    byggesakSignaturesbl.Add(signatur);
                }
                else
                {
                    byggesakSignatures.Add(signatur);
                }
            }

            var signaturesDicvtionary = new Dictionary<string, List<byggesakSignature>>()
            {
                {"blockedreason", byggesakSignaturesbl},
                {"signertNaa", byggesakSignatures}
            };
            return signaturesDicvtionary;
        }

        public JArray GetFtbTiltaksType()
        {
            JArray jsonArray = null;
            try
            {
                var formDataAsXml = File.ReadAllText(@"Data\ByggesoknadTiltaktype_19_09_20.json", Encoding.UTF8);
                jsonArray = JArray.Parse(formDataAsXml);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Can't open ByggesoknadTiltaktype_19_09_20.json file");
            }
            return jsonArray;
        }

        public Dictionary<string, string> GetFtbTiltaksTypeFrombyggsokTiltaksArt(string[] tiltaksarts)
        {
            Dictionary<string, string> tiltakstypes = null;
            if (!Helpers.ObjectIsNullOrEmpty(tiltaksarts))
            {
                tiltakstypes = new Dictionary<string, string>();
                var ftbTiltakstypes = GetFtbTiltaksType();
                foreach (var tiltaksart in tiltaksarts)
                {
                    try
                    {
                        var titaksartCode = tiltaksart.Contains("Item") ? tiltaksart.Substring("Item".Length) : tiltaksart;
                        if (ftbTiltakstypes.Any(t => t["ByggsokTiltaksart"].ToString() == titaksartCode))
                        {
                            var ftbTiltakType = ftbTiltakstypes.First(t => t["ByggsokTiltaksart"].ToString() == titaksartCode);
                            tiltakstypes.Add(ftbTiltakType["Kodeverdi"].ToString(), ftbTiltakType["Navn"].ToString());
                        }
                        else
                        {
                            _logger.LogDebug("Finnes ikke Tiktaksar ('{tiltaksart}')i FTB tiltakstype kodeliste ", titaksartCode);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, "Feil ved konvertering av tiltaksart ('{tiltaksart}')", tiltaksart);
                    }
                }
            }
            return tiltakstypes;
        }

        public bool ErBerorerarbeidsplasser(tiltaketsramme[] tiltaketsramme)
        {
            bool berorerarbeidsplasser = false;

            if (tiltaketsramme != null && tiltaketsramme.Any())
            {
                berorerarbeidsplasser = tiltaketsramme.Any(t => t.generellevilkar?.berorerarbeidsplasser == true);
            }
            return berorerarbeidsplasser;
        }

        public Dictionary<string, List<byggesakSignature>> GetDictionaryOfSignatureBysoknadtype(List<byggesakSignature> byggesakSignatures)
        {
            Dictionary<string, List<byggesakSignature>> dictionary = new Dictionary<string, List<byggesakSignature>>();

            if (byggesakSignatures != null && byggesakSignatures.Any())
            {
                foreach (byggesakSignature signature in byggesakSignatures)
                {
                    var lastSignaturType = GetSoknadTypeFrombygsokSignatur(signature);

                    if (!string.IsNullOrEmpty(lastSignaturType))
                    {
                        if (dictionary.ContainsKey(lastSignaturType))
                        {
                            dictionary[lastSignaturType].Add(signature);
                        }
                        else
                        {
                            dictionary.Add(lastSignaturType, new List<byggesakSignature>() { signature });
                        }
                    }
                }
            }
            return dictionary;
        }
    }
}