﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace dibk.ftb.byggsok.Model.Api
{
    public class ConvertFormTaskResponse
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("serviceCode")]
        public string ServiceCode { get; set; }
        [JsonProperty("serviceEdition")]
        public string ServiceEdition { get; set; }
        [JsonProperty("_embedded")]
        public Embedded EmbeddedData { get; set; }
    }

    public class Embedded
    {
        [JsonProperty("forms")]
        public List<Form> Forms { get; set; }
    }
}