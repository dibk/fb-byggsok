export const removeLineBreaks = string => {
    return string.replace(/\r?\n|\r/g, "")
}

export const removeTabCharacters = string => {
    return string.replace(/\t+/g, "")
}
