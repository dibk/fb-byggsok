﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using dibk.ftb.byggsok.Services;
using dibk.ftb.byggsok.Util;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Xunit;

namespace dibk.ftb.byggsok.test
{
   public class ConvertMidlertidigbrukstillatelseTests
    {
        readonly ILogger<ConvertMidlertidigbrukstillatelse> _logger = new Logger<ConvertMidlertidigbrukstillatelse>(new NullLoggerFactory());

            [Fact(DisplayName = "Kommplet - ByggesakDebug_5 OK")]
        public void MidlertidigbrukstillatelseByggesakDebug_5OkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_5.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var midlertidigbrukstillatelseV2Type = new ConvertMidlertidigbrukstillatelse(_logger).Convert(byggsok);
            midlertidigbrukstillatelseV2Type.ansvarligSoeker.Should().NotBeNull();
            midlertidigbrukstillatelseV2Type.tiltakshaver.Should().NotBeNull();
            midlertidigbrukstillatelseV2Type.eiendomByggested.Should().NotBeNull();
        }
        [Fact(DisplayName = "Kommplet - ByggesakDebug_7 OK")]
        public void MidlertidigbrukstillatelseByggesakDebug_7OkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_7.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var midlertidigbrukstillatelseV2Type = new ConvertMidlertidigbrukstillatelse(_logger).Convert(byggsok);
            midlertidigbrukstillatelseV2Type.ansvarligSoeker.Should().NotBeNull();
            midlertidigbrukstillatelseV2Type.tiltakshaver.Should().NotBeNull();
            midlertidigbrukstillatelseV2Type.eiendomByggested.Should().NotBeNull();
        }
        [Fact(DisplayName = "Kommplet - ByggesakDebug_9 OK")]
        public void MidlertidigbrukstillatelseByggesakDebug_9OkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_9.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var midlertidigbrukstillatelseV2Type = new ConvertMidlertidigbrukstillatelse(_logger).Convert(byggsok);
            midlertidigbrukstillatelseV2Type.ansvarligSoeker.Should().NotBeNull();
            midlertidigbrukstillatelseV2Type.tiltakshaver.Should().NotBeNull();
            midlertidigbrukstillatelseV2Type.eiendomByggested.Should().NotBeNull();
        }
        [Fact(DisplayName = "Kommplet - ByggesakDebug_hilde OK")]
        public void MidlertidigbrukstillatelseByggesakDebug_hildeOkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var midlertidigbrukstillatelseV2Type = new ConvertMidlertidigbrukstillatelse(_logger).Convert(byggsok);
            midlertidigbrukstillatelseV2Type.ansvarligSoeker.Should().NotBeNull();
            midlertidigbrukstillatelseV2Type.tiltakshaver.Should().NotBeNull();
            midlertidigbrukstillatelseV2Type.eiendomByggested.Should().NotBeNull();
        }
    }
}
