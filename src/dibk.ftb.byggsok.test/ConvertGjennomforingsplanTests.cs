﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using dibk.ftb.byggsok.Services;
using dibk.ftb.byggsok.Util;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using no.kxml.skjema.dibk.gjennomforingsplanV6;
using Xunit;

namespace dibk.ftb.byggsok.test
{
    public class ConvertGjennomforingsplanTests
    {
        readonly ILogger<ConvertGjennomforingsplan> _logger = new Logger<ConvertGjennomforingsplan>(new NullLoggerFactory());

        [Fact(DisplayName = "Kommplet - ByggesakDebug_hilde OK", Skip = "Save Xml to local Disc")]
        public void ConvertGjennomforingsplan_ByggesakDebug_hildeOkTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);

            var gjennomfoeringsplanType = new ConvertGjennomforingsplan(_logger).Convert(byggsok);


            gjennomfoeringsplanType.ansvarligSoeker.Should().NotBeNull();
            gjennomfoeringsplanType.eiendomByggested.Should().NotBeNull();
            gjennomfoeringsplanType.gjennomfoeringsplan.Any().Should().BeTrue();

            var savedFile = TestHelper.SaveSchema2XmlFile(gjennomfoeringsplanType, $"{gjennomfoeringsplanType.GetType().Name}{gjennomfoeringsplanType.dataFormatId}");
            savedFile.Should().BeTrue();
        }

        [Fact(DisplayName = "Kommplet - ByggesakDebug (4)")]
        public void ConvertGjennomforingsplan_ByggesakDebug4OkTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug (4).xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);

            var gjennomfoeringsplanType = new ConvertGjennomforingsplan(_logger).Convert(byggsok);


            gjennomfoeringsplanType.Should().NotBeNull();
        }

        [Fact(DisplayName = "AddSaksnummerType OK", Skip = "bug to read xml can't get saksid.ref")]
        public void AddSaksnummerTypeOkTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug (4).xml");
            var stringnnn = TestHelper.GetXmlWithoutSpaces(byggsokxml);

            var resultForm = new no.kxml.skjema.dibk.gjennomforingsplanV6.GjennomfoeringsplanType();
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var saksnummer = byggsok.saksid.FirstOrDefault()?.eier;

            new ConvertGjennomforingsplan(_logger).AddkommunensSaksnummerAar(byggsok, resultForm);

            resultForm.kommunensSaksnummer.saksaar.Should().NotBeEmpty();
            resultForm.kommunensSaksnummer.sakssekvensnummer.Should().NotBeEmpty();
        }

        [Fact(DisplayName = "ConvertPartTypeFromByggsokToFtb OK")]
        public void ConvertPartTypeFromByggsokToFtbOkTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_5.xml");
            //new ConvertGjennomforingsplan(_logger).Convert(byggsokxml);
            var resultForm = new no.kxml.skjema.dibk.gjennomforingsplanV6.GjennomfoeringsplanType();
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var ftbAnsvarligSoeker = new ByggsokUtilities(_logger).GetbyggsokPartype("ID_1484231270949_82535", byggsok);
            var foretak = new ConvertGjennomforingsplan(_logger).ConvertPartTypeFromByggsokToFtb(ftbAnsvarligSoeker);


            foretak.Should().NotBeNull();
        }  
        [Fact(DisplayName = "ConvertPartTypeFromByggsokToFtb offentligmyndighet OK")]
        public void ConvertPartTypeFromByggsokToFtbOffentligmyndighetOkTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug (4).xml");
            //new ConvertGjennomforingsplan(_logger).Convert(byggsokxml);
            var resultForm = new no.kxml.skjema.dibk.gjennomforingsplanV6.GjennomfoeringsplanType();
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var ftbAnsvarligSoeker = new ByggsokUtilities(_logger).GetbyggsokPartype("ID_1502889166503_8465", byggsok);
            var offentligmyndighet = new ConvertGjennomforingsplan(_logger).ConvertPartTypeFromByggsokToFtb(ftbAnsvarligSoeker);


            offentligmyndighet.Should().NotBeNull();
        }
        [Fact(DisplayName = "ConvertPartTypeFromByggsokToFtb privatperson OK")]
        public void ConvertPartTypeFromByggsokToFtbPrivatpersonOkTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            //new ConvertGjennomforingsplan(_logger).Convert(byggsokxml);
            var resultForm = new no.kxml.skjema.dibk.gjennomforingsplanV6.GjennomfoeringsplanType();
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var ftbAnsvarligSoeker = new ByggsokUtilities(_logger).GetbyggsokPartype("ID_1515414803085_2", byggsok);
            var privatperson = new ConvertGjennomforingsplan(_logger).ConvertPartTypeFromByggsokToFtb(ftbAnsvarligSoeker);


            privatperson.Should().NotBeNull();
        }

        [Fact(DisplayName = "GetEiendomByggestedFromByggesak OK")]
        public void GetEiendomByggestedFromByggesakOkTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug (4).xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);

            var eiendomType = new ConvertGjennomforingsplan(_logger).GetEiendomByggestedFromByggesak(byggsok);

            eiendomType.Should().NotBeNull();
        }

        [Fact(DisplayName = "ConvertByggsokPartTypeToForetakType OK")]
        public void ConvertByggsokPartTypeToForetakTypeOkTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug (4).xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var utfAnsvarligForArbeidTypes = new ByggsokUtilities(_logger).GetAnsvarligForArbeidUTF(byggsok);
            var ftbAnsvarligSoeker = new ByggsokUtilities(_logger).GetbyggsokPartype(utfAnsvarligForArbeidTypes[0].ansvarlig.@ref, byggsok);
            var foretak = new ConvertGjennomforingsplan(_logger).ConvertByggsokPartTypeToForetakType(ftbAnsvarligSoeker);

            foretak.partstype.Should().NotBeNull();
            foretak.organisasjonsnummer.Should().NotBeNull();
            foretak.kontaktperson.Should().NotBeNull();
            foretak.navn.Should().NotBeNull();
            foretak.epost.Should().NotBeNull();
            foretak.mobilnummer.Should().NotBeNull();
            foretak.adresse.Should().NotBeNull();

            foretak.Should().NotBeNull();
        }

        [Fact(DisplayName = "SetGjennomforingsplanDateTime OK")]
        public void SetGjennomforingsplanDateTimeOkTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug (4).xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            AnsvarsomraadeType ansvarsomraade = new AnsvarsomraadeType();
            var utfAnsvarligForArbeidTypes = new ByggsokUtilities(_logger).GetAnsvarligForArbeidUTF(byggsok);
            var ftbAnsvarligSoeker = new ByggsokUtilities(_logger).GetbyggsokPartype(utfAnsvarligForArbeidTypes[0].ansvarlig.@ref, byggsok);
            new ConvertGjennomforingsplan(_logger).SetGjennomforingsplanDateTime("midlertidigbrukstillatelse", DateTime.Today, ansvarsomraade);

            ansvarsomraade.samsvarKontrollForeliggerVedMidlertidigBrukstillatelse = DateTime.Today;
        }

        [Fact(DisplayName = "ConvertUtfAnsvarligForArbeidToFtb OK")]
        public void ConvertUtfAnsvarligForArbeidToFtbOkTest()
        {
           
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug (4).xml");
            var xmlSring = TestHelper.GetXmlWithoutSpaces(byggsokxml);
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);

            var utfAnsvarligForArbeidTypes = new ByggsokUtilities(_logger).GetAnsvarligForArbeidUTF(byggsok);


            var ansvarsomraade = new ConvertGjennomforingsplan(_logger).ConvertUtfAnsvarligForArbeidToFtb(utfAnsvarligForArbeidTypes[2], byggsok); //[3] ByggesakDebug_5.

            ansvarsomraade.Should().NotBeNull();

        } 
        [Fact(DisplayName = "ConvertUtfAnsvarligForArbeidToFtb OK")]
        public void ConvertProAnsvarligForArbeidToFtbOkTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug (4).xml");
            var xmlSring = TestHelper.GetXmlWithoutSpaces(byggsokxml);
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);

            var ansvarligForArbeidPro = new ByggsokUtilities(_logger).GetAnsvarligForArbeidPRO(byggsok);
            var ansvarsomraade = new ConvertGjennomforingsplan(_logger).Convert(byggsok);

            ansvarsomraade.Should().NotBeNull();

        }
    }
}
