﻿using dibk.ftb.byggsok.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Enumeration;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;

namespace dibk.ftb.byggsok.test
{
    public class TestHelper
    {
        public static bool SaveSchema2XmlFile(object form,string fileName = null, string path = null)
        {
            try
            {
                var gjennomforingsplanXml = SerializeUtil.Serialize(form);

                path = path ?? Path.Combine(@"C:\Temp\", $"{fileName}.xml");
                File.WriteAllText(path, gjennomforingsplanXml);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public static string GetXmlWithoutSpaces(string formAsXml)
        {
            Regex Parser = new Regex(@">\s*<");
            var xml = Parser.Replace(formAsXml, "><");
            xml.Trim();
            return xml;
        }

        public static IConfiguration CreateMemoryConfigurationBuilder()
        {
            var myConfiguration = new Dictionary<string, string>
            {
                {"SendgridApiKey","***"},
            };

            var configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(myConfiguration)
                .Build();
            return configuration;
        }
    }
}
