﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace dibk.ftb.byggsok.Logging
{
    public static class LogCorrelationMiddlewareExtensions
    {
        public static IApplicationBuilder UseLogCorrelationIdMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<LogCorrelationIdMiddleware>();
        }
    }

    public class LogCorrelationIdMiddleware
    {
        private readonly RequestDelegate _next;

        public LogCorrelationIdMiddleware(RequestDelegate next)
        {
            this._next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var correlationId = GetCorrelationId(context);

            var logger = context.RequestServices.GetRequiredService<ILogger<LogCorrelationIdMiddleware>>();
            using (logger.BeginScope("{@CorrelationId}", correlationId))
            {
                await this._next(context);
            }
        }

        public string GetCorrelationId(HttpContext context)
        {
            var header = context.Request.Headers["x-correlation-id"];
            var correlationId = string.Empty;

            if (header.Count > 0)
            {
                correlationId = header[0];
            }

            return correlationId;
        }
    }
}




