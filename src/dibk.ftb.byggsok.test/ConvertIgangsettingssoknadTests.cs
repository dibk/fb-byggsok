﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using dibk.ftb.byggsok.Services;
using dibk.ftb.byggsok.Util;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Xunit;

namespace dibk.ftb.byggsok.test
{

    public class ConvertIgangsettingssoknadTests
    {
        readonly ILogger<ConvertIgangsettingssoknad> _logger = new Logger<ConvertIgangsettingssoknad>(new NullLoggerFactory());

        [Fact(DisplayName = "Kommplet - ByggesakDebug_5 OK")]
        public void IgangsettingstillatelseByggesakDebug_5OkTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_5.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var igangsettingstillatelse = new ConvertIgangsettingssoknad(_logger).Convert(byggsok);
            igangsettingstillatelse.ansvarligSoeker.Should().NotBeNull();
            igangsettingstillatelse.eiendomByggested.Should().NotBeNull();
        }
        [Fact(DisplayName = "Kommplet - ByggesakDebug_7 OK")]
        public void IgangsettingstillatelseByggesakDebug_7OkTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_7.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var igangsettingstillatelse = new ConvertIgangsettingssoknad(_logger).Convert(byggsok);
            igangsettingstillatelse.ansvarligSoeker.Should().NotBeNull();
            igangsettingstillatelse.eiendomByggested.Should().NotBeNull();
        }
        [Fact(DisplayName = "Kommplet - ByggesakDebug_9 OK")]
        public void IgangsettingstillatelseByggesakDebug_9OkTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_9.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var igangsettingstillatelse = new ConvertIgangsettingssoknad(_logger).Convert(byggsok);
            igangsettingstillatelse.ansvarligSoeker.Should().NotBeNull();
            igangsettingstillatelse.eiendomByggested.Should().NotBeNull();
        }
        [Fact(DisplayName = "Kommplet - ByggesakDebug_hilde OK")]
        public void IgangsettingstillatelseByggesakDebug_hildeOkTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var igangsettingstillatelse = new ConvertIgangsettingssoknad(_logger).Convert(byggsok);
            igangsettingstillatelse.ansvarligSoeker.Should().NotBeNull();
            igangsettingstillatelse.eiendomByggested.Should().NotBeNull();
        } 
        [Fact(DisplayName = "IG - ErBerorerarbeidsplasser OK")]
        public void IgangsettingstillatelseErBerorerarbeidsplasserOkTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_5.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var igangsettingstillatelse = new ConvertIgangsettingssoknad(_logger).Convert(byggsok);

            igangsettingstillatelse.beroererArbeidsplasser.Should().BeTrue();
        }
    }
}
