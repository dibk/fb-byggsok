import { FETCH_AVAILABLE_REPORTEES } from 'constants/types';

const initialState = []

export default function(state = initialState, action) {
	switch(action.type) {
		case FETCH_AVAILABLE_REPORTEES:
			return action.payload;
		default:
			return state;
	}

}
