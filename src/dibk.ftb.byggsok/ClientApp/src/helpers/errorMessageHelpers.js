export const getStatusError = (response) => {
    return {
        status: response.status,
        statusText: response.statusText,
        url: response.url
    }
}