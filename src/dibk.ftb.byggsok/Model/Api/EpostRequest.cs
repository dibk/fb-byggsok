﻿using System.ComponentModel.DataAnnotations;

namespace dibk.ftb.byggsok.Model.Api
{
    public class EpostRequest
    {
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string email { get; set; }

        public string emailSenderName { get; set; }
        public string subject { get; set; }
        public string purpose { get; set; }
        public string messageUrl { get; set; }
        public string altinArchiveReferenceNumber { get; set; }
    }
}