using dibk.ftb.byggsok.Services;
using System;
using System.IO;
using System.Linq;
using dibk.ftb.byggsok.Util;
using Xunit;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

namespace dibk.ftb.byggsok.test
{
    public class ByggsokToFtbServiceUnitTests
    {
        readonly ILogger<ConvertGjennomforingsplan> _logger = new Logger<ConvertGjennomforingsplan>(new NullLoggerFactory());
        [Fact]
        public void GjennomforingsplanAnsvarProsjekteringTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);

            var form = new ConvertGjennomforingsplan(_logger).Convert(byggsok);

            //TODO sjekke detaljer er konvertert riktig
            form.gjennomfoeringsplan.Length.Should().BeGreaterThan(0);

        }

        [Fact]
        public void GjennomforingsplanAnsvarKontrollTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var form = new ConvertGjennomforingsplan(_logger).Convert(byggsok);

            //TODO sjekke detaljer er konvertert riktig
            form.gjennomfoeringsplan.Length.Should().BeGreaterThan(0);

        }

        [Fact]
        public void GjennomforingsplanAnsvarligSokerTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var form = new ConvertGjennomforingsplan(_logger).Convert(byggsok);

            var utf = form.gjennomfoeringsplan.Where(g => g.funksjon.kodeverdi == "UTF").ToArray();
            var pro = form.gjennomfoeringsplan.Where(g => g.funksjon.kodeverdi == "PRO").ToArray();
            var kontroll = form.gjennomfoeringsplan.Where(g => g.funksjon.kodeverdi == "KONTROLL").ToArray();

            form.eiendomByggested.Count().Should().Be(1);

            utf.Count().Should().Be(0);
            pro.Count().Should().Be(1);
            kontroll.Count().Should().Be(0);

            utf.Count(a => !string.IsNullOrEmpty(a.foretak.navn)).Should().Be(0);
            pro.Count(a => !string.IsNullOrEmpty(a.foretak.navn)).Should().Be(1);
            kontroll.Count(a => !string.IsNullOrEmpty(a.foretak.navn)).Should().Be(0);



            pro.GroupBy(a => a.foretak.partstype.kodeverdi).Should().NotBeNull();


            //TODO sjekke detaljer er konvertert riktig
            form.ansvarligSoeker.Should().NotBeNull();

        }
        [Fact]
        public void GjennomforingsplanAnsvarligUtforendeTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_5.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var form = new ConvertGjennomforingsplan(_logger).Convert(byggsok);


            //TODO sjekke detaljer er konvertert riktig
            var utf = form.gjennomfoeringsplan.Where(g => g.funksjon.kodeverdi == "UTF").ToArray();
            var pro = form.gjennomfoeringsplan.Where(g => g.funksjon.kodeverdi == "PRO").ToArray();
            var kontroll = form.gjennomfoeringsplan.Where(g => g.funksjon.kodeverdi == "KONTROLL").ToArray();

            utf.Count().Should().Be(65);
            pro.Count().Should().Be(32);
            kontroll.Count().Should().Be(8);
            var nokojhjh = utf.Where(a => a.foretak == null).ToArray();
            utf.Count(a => !string.IsNullOrEmpty(a.foretak.navn)).Should().Be(65);
            pro.Count(a => !string.IsNullOrEmpty(a.foretak.navn)).Should().Be(32);
            kontroll.Count(a => !string.IsNullOrEmpty(a.foretak.navn)).Should().Be(8);
            var noko = utf.GroupBy(a => a.foretak.partstype.kodeverdi).Select(gr => gr.Key);
            var noko2 = pro.GroupBy(a => a.foretak.partstype.kodeverdi).Select(gr => gr.Key);
            var noko3 = kontroll.GroupBy(a => a.foretak.partstype.kodeverdi).Select(gr => gr.Key);

            form.gjennomfoeringsplan.Length.Should().BeGreaterThan(0);

        }

        [Fact]
        public void GjennomforingsplanAnsvarlig7Test()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_5.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var form = new ConvertGjennomforingsplan(_logger).Convert(byggsok);

            var utf = form.gjennomfoeringsplan.Where(g => g.funksjon.kodeverdi == "UTF").ToArray();
            var pro = form.gjennomfoeringsplan.Where(g => g.funksjon.kodeverdi == "PRO").ToArray();
            var kontroll = form.gjennomfoeringsplan.Where(g => g.funksjon.kodeverdi == "KONTROLL").ToArray();

            utf.Count().Should().Be(65);
            pro.Count().Should().Be(32);
            kontroll.Count().Should().Be(8);

            utf.Count(a => !string.IsNullOrEmpty(a.foretak.navn)).Should().Be(65);
            pro.Count(a => !string.IsNullOrEmpty(a.foretak.navn)).Should().Be(32);
            kontroll.Count(a => !string.IsNullOrEmpty(a.foretak.navn)).Should().Be(8);

            //var funksjonUtf = utf[5].funksjon;
            var funksjonPro = pro[5].funksjon;
            var funksjonKontroll = kontroll[0].funksjon;

            //TODO sjekke detaljer er konvertert riktig
            form.gjennomfoeringsplan.Length.Should().BeGreaterThan(0);

        }


        [Fact]
        public void GjennomforingsplanAnsvarlig9Test()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_9.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var form = new ConvertGjennomforingsplan(_logger).Convert(byggsok);


            var utf = form.gjennomfoeringsplan.Where(g => g.funksjon.kodeverdi == "UTF").ToArray();
            var pro = form.gjennomfoeringsplan.Where(g => g.funksjon.kodeverdi == "PRO").ToArray();
            var kontroll = form.gjennomfoeringsplan.Where(g => g.funksjon.kodeverdi == "KONTROLL").ToArray();

            utf.Count().Should().Be(1);
            pro.Count().Should().Be(1);
            kontroll.Count().Should().Be(0);

            utf.Count(a => !string.IsNullOrEmpty(a.foretak.navn)).Should().Be(1);
            pro.Count(a => !string.IsNullOrEmpty(a.foretak.navn)).Should().Be(1);
            kontroll.Count(a => !string.IsNullOrEmpty(a.foretak.navn)).Should().Be(0);

            //TODO sjekke detaljer er konvertert riktig
            form.gjennomfoeringsplan.Length.Should().BeGreaterThan(0);

        }

       
        [Fact]
        public void IGTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\IG-5.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var form = new ConvertGjennomforingsplan(_logger).Convert(byggsok);


            //TODO sjekke detaljer er konvertert riktig
            form.gjennomfoeringsplan.Length.Should().BeGreaterThan(0);

            var formIG = new ConvertIgangsettingssoknad(_logger).Convert(byggsok);

            //TODO sjekke detaljer er konvertert riktig
            formIG.delsoeknader.Length.Should().BeGreaterThan(0);

        }

        [Fact]
        public void FraEttTrinnTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggSøk xml ettrinnssøknad.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var form = new ConvertGjennomforingsplan(_logger).Convert(byggsok);


            //TODO sjekke detaljer er konvertert riktig
            form.gjennomfoeringsplan.Length.Should().BeGreaterThan(0);

            var formIG = new ConvertMidlertidigbrukstillatelse(_logger).Convert(byggsok);

            //TODO sjekke detaljer er konvertert riktig
            formIG.eiendomByggested.Length.Should().BeGreaterThan(0);

        }

        [Fact]
        public void FraEttTrinnTest2()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug (1).xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var form = new ConvertGjennomforingsplan(_logger).Convert(byggsok);


            //TODO sjekke detaljer er konvertert riktig
            form.gjennomfoeringsplan.Length.Should().BeGreaterThan(0);

            var formIG = new ConvertFerdigattest(_logger).Convert(byggsok);

            //TODO sjekke detaljer er konvertert riktig
            formIG.eiendomByggested.Length.Should().BeGreaterThan(0);

        }

        [Fact]
        public void FraEttTrinnTest3()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug (2).xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var form = new ConvertGjennomforingsplan(_logger).Convert(byggsok);


            //TODO sjekke detaljer er konvertert riktig
            form.gjennomfoeringsplan.Length.Should().BeGreaterThan(0);

            var formIG = new ConvertFerdigattest(_logger).Convert(byggsok);

            //TODO sjekke detaljer er konvertert riktig
            formIG.eiendomByggested.Length.Should().BeGreaterThan(0);

        }

        [Fact]
        public void FraMBTest()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug (4).xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var form = new ConvertGjennomforingsplan(_logger).Convert(byggsok);


            //TODO sjekke detaljer er konvertert riktig
            form.gjennomfoeringsplan.Length.Should().BeGreaterThan(0);

            var formIG = new ConvertMidlertidigbrukstillatelse(_logger).Convert(byggsok);

            //TODO sjekke detaljer er konvertert riktig
            formIG.delsoeknad.Length.Should().BeGreaterThan(0);

        }


    }
}
