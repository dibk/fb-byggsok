using dibk.ftb.byggsok.Util;
using Microsoft.Extensions.Logging;
using no.dibk.byggsok;
using no.kxml.skjema.dibk.gjennomforingsplanV6;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dibk.ftb.byggsok.Services
{
    public class ConvertGjennomforingsplan
    {
        private ILogger _logger;
        private ByggsokUtilities _byggsokUtilities;

        public ConvertGjennomforingsplan(ILogger logger)
        {
            _logger = logger;
            _byggsokUtilities = new ByggsokUtilities(_logger);
        }

        public no.kxml.skjema.dibk.gjennomforingsplanV6.GjennomfoeringsplanType Convert(byggesak byggsok)
        {
            var resultForm = new no.kxml.skjema.dibk.gjennomforingsplanV6.GjennomfoeringsplanType();
            _logger.LogDebug("Start Gjennomfoeringsplan convertering");

            if (byggsok == null)
                return resultForm;

            resultForm.metadata = new MetadataType()
            {
                fraSluttbrukersystem = "Byggsøk",
                prosjektnavn = "",
                hovedinnsendingsnummer = byggsok.soknadid
            };

            resultForm.versjon = "";

            // saksaar and sakssekvensnummer
            AddkommunensSaksnummerAar(byggsok, resultForm);

            //Byggeeiendom
            resultForm.eiendomByggested = GetEiendomByggestedFromByggesak(byggsok);

            var gjennomforingsplan = new List<AnsvarsomraadeType>();

            //Ansvarsfordeling UTF
            List<AnsvarsomraadeType> gjennomforingsplanUtf = null;
            var utfAnsvarligForArbeidTypes = _byggsokUtilities.GetAnsvarligForArbeidUTF(byggsok);
            if (!Helpers.ObjectIsNullOrEmpty(utfAnsvarligForArbeidTypes))
            {
                gjennomforingsplanUtf = new List<AnsvarsomraadeType>();
                foreach (var utf in utfAnsvarligForArbeidTypes)
                {
                    var ansvar = ConvertUtfAnsvarligForArbeidToFtb(utf, byggsok);
                    if (ansvar != null)
                        gjennomforingsplanUtf.Add(ansvar);
                }
            }

            //Ansvarsfordeling PRO
            List<AnsvarsomraadeType> gjennomforingsplanPro = null;
            var proAnsvarligForArbeidTypes = _byggsokUtilities.GetAnsvarligForArbeidPRO(byggsok);
            if (!Helpers.ObjectIsNullOrEmpty(proAnsvarligForArbeidTypes))
            {
                gjennomforingsplanPro = new List<AnsvarsomraadeType>();
                foreach (var pro in proAnsvarligForArbeidTypes)
                {
                    var ansvar = ConvertProAnsvarligForArbeidToFtb(pro, byggsok);
                    if (ansvar != null)
                        gjennomforingsplanPro.Add(ansvar);
                }
            }

            //Ansvarsfordeling KONTROLL
            List<AnsvarsomraadeType> gjennomforingsplanKon = null;
            var konAnsvarligForArbeidTypes = _byggsokUtilities.GetAnsvarligForArbeidKON(byggsok);
            if (!Helpers.ObjectIsNullOrEmpty(konAnsvarligForArbeidTypes))
            {
                gjennomforingsplanKon = new List<AnsvarsomraadeType>();
                foreach (var kon in konAnsvarligForArbeidTypes)
                {
                    var ansvar = ConvertKonAnsvarligForArbeidToFtb(kon, byggsok);
                    if (ansvar != null)
                        gjennomforingsplanKon.Add(ansvar);
                }
            }

            if (gjennomforingsplanUtf != null)
                gjennomforingsplan.AddRange(gjennomforingsplanUtf);

            // TODO review these methods
            if (gjennomforingsplanPro != null)
                gjennomforingsplan.AddRange(gjennomforingsplanPro);
            if (gjennomforingsplanKon != null)
                gjennomforingsplan.AddRange(gjennomforingsplanKon);

            //If is any "gjennomforingsplan" add to model "byggsøk xml" can be sent without any information
            if (gjennomforingsplan.Any())
                resultForm.gjennomfoeringsplan = gjennomforingsplan.ToArray();

            //Ansvarlig søker
            try
            {
                var ftbAnsvarligSoeker = new ByggsokUtilities(_logger).GetAnsvarligSokerParsType(byggsok);
                resultForm.ansvarligSoeker = ConvertPartTypeFromByggsokToFtb(ftbAnsvarligSoeker);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Feil ved konvertering av ansvarlig søker for Gjennomforingsplan");
            }

            return resultForm;
        }

        public void AddkommunensSaksnummerAar(byggesak byggsok, GjennomfoeringsplanType resultForm)
        {
            if (byggsok.identifier != null)
            {
                var municipalityInfo = new ByggsokUtilities(_logger).GetMunicipalityYaerAndNumber(byggsok);
                if (municipalityInfo.Any())
                {
                    resultForm.kommunensSaksnummer = new SaksnummerType();
                    resultForm.kommunensSaksnummer.saksaar = municipalityInfo["saksaar"].ToString();
                    resultForm.kommunensSaksnummer.sakssekvensnummer = municipalityInfo["sakssekvensnummer"].ToString();
                }
            }
        }

        /// <summary>
        /// Get eiendoms from Byggesak model
        /// </summary>
        /// <param name="byggsok"></param>
        /// <returns></returns>
        public EiendomType[] GetEiendomByggestedFromByggesak(byggesak byggsok)
        {
            var eiendombyggested = new List<EiendomType>();

            if (!Helpers.ObjectIsNullOrEmpty(byggsok?.eiendom))
            {
                foreach (var bseiendom in byggsok.eiendom)
                {
                    if (bseiendom.typeSpecified && bseiendom.type == no.dibk.byggsok.eiendomType.byggeeiendom)
                    {
                        var eiendomType = new EiendomType();
                        if (!Helpers.ObjectIsNullOrEmpty(bseiendom.eiendomsidentifikasjon))
                        {
                            eiendomType.eiendomsidentifikasjon = new MatrikkelnummerType();
                            eiendomType.eiendomsidentifikasjon.kommunenummer = bseiendom.eiendomsidentifikasjon.kommunenummer;
                            eiendomType.eiendomsidentifikasjon.gaardsnummer = bseiendom.eiendomsidentifikasjon.gardsnummer;
                            eiendomType.eiendomsidentifikasjon.bruksnummer = bseiendom.eiendomsidentifikasjon.bruksnummer;
                            eiendomType.eiendomsidentifikasjon.festenummer = bseiendom.eiendomsidentifikasjon.festenummer;
                            eiendomType.eiendomsidentifikasjon.seksjonsnummer = bseiendom.eiendomsidentifikasjon.seksjonsnummer;
                        }

                        var adresse = bseiendom.adresse.First();
                        if (!Helpers.ObjectIsNullOrEmpty(adresse))
                        {
                            //Tar bare med første adresse fra byggsøk
                            eiendomType.adresse = new EiendommensAdresseType();
                            var bsAdresseType = adresse.adresselinje?.FirstOrDefault();
                            if (!string.IsNullOrEmpty(bsAdresseType?.Item?.ToString()))
                                eiendomType.adresse.adresselinje1 = bsAdresseType.Item?.ToString();

                            eiendomType.adresse.postnr = adresse.postnummer;
                            eiendomType.adresse.poststed = adresse.poststed;
                        }
                        eiendombyggested.Add(eiendomType);
                    }
                }
            }
            return eiendombyggested.ToArray();
        }

        /// <summary>
        /// Convert Byggsøk "UtfAnsvarligForArbeidType" to Ftb "AnsvarsomraadeType"
        /// </summary>
        /// <param name="utf"></param>
        /// <param name="byggsok"></param>
        /// <returns></returns>
        public AnsvarsomraadeType ConvertUtfAnsvarligForArbeidToFtb(UtfAnsvarligForArbeidType utf, byggesak byggsok)
        {
            if (Helpers.ObjectIsNullOrEmpty(utf) || Helpers.ObjectIsNullOrEmpty(byggsok))
                return null;

            AnsvarsomraadeType ansvar = null;

            ansvar = new AnsvarsomraadeType()
            {
                funksjon = new KodeType()
                {
                    kodeverdi = "UTF",
                    kodebeskrivelse = "Ansvarlig utførelse"
                },
                ansvarsomraade = utf.beskrivelseansvarsomrade
            };

            if (utf.tiltaksklasseSpecified)
            {
                var tiltaklasseKodeverdi = (int)utf.tiltaksklasse + 1;
                ansvar.tiltaksklasse = new KodeType()
                {
                    kodeverdi = tiltaklasseKodeverdi.ToString(),
                    kodebeskrivelse = tiltaklasseKodeverdi.ToString(),
                };
            }
            //Avkryssninger
            if (utf.ferdigatSpecified) ansvar.samsvarKontrollPlanlagtVedFerdigattest = utf.ferdigat;
            if (utf.soknadommidlerSpecified) ansvar.samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = utf.soknadommidler;
            if (utf.soknadomigangSpecified) ansvar.samsvarKontrollPlanlagtVedIgangsettingstillatelse = utf.soknadomigang;
            if (utf.soknadomrammeSpecified) ansvar.samsvarKontrollPlanlagtVedRammetillatelse = utf.soknadomramme;

            //legge inn partType foretak
            var ftbAnsvarligSoeker = _byggsokUtilities.GetbyggsokPartype(utf.ansvarlig?.@ref, byggsok);
            ansvar.foretak = ConvertByggsokPartTypeToForetakType(ftbAnsvarligSoeker);

            //Get all samsvarserklering/utforelse "SamsvarserkleringUtforelse"
            var utfAnsvarligForArbeiId = utf.id;
            var samsvarserkleringUtforelses = _byggsokUtilities.GetSamsvarserkleringUtforelse(utfAnsvarligForArbeiId, byggsok);

            //select only the ones that are done
            byggesakSignature lastSignaturDateTime = null;

            if (samsvarserkleringUtforelses != null && samsvarserkleringUtforelses.Any())
            {
                samsvarserkleringUtforelse samsvarserkleringUtf = null;

                // Select last Utf
                if (samsvarserkleringUtforelses.Length == 1)
                {
                    samsvarserkleringUtf = samsvarserkleringUtforelses.FirstOrDefault();
                    var byggesakSignature = _byggsokUtilities.GetByggesakSignatureFromRefId(samsvarserkleringUtf?.signatureref.@ref, byggsok);
                    if (byggesakSignature != null)
                    {
                        var lastSignaturType = _byggsokUtilities.GetSoknadTypeFrombygsokSignatur(byggesakSignature);
                        SetGjennomforingsplanDateTime(lastSignaturType, byggesakSignature?.signeddate, ansvar);
                    }
                }
                else
                {
                    List<byggesakSignature> signaturList = new List<byggesakSignature>();
                    foreach (samsvarserkleringUtforelse samsvarserkleringUtforelse in samsvarserkleringUtforelses)
                    {
                        var signature = _byggsokUtilities.GetByggesakSignatureFromRefId(samsvarserkleringUtforelse.signatureref.@ref, byggsok);
                        signaturList.Add(signature);
                    }

                    if (signaturList.Any())
                    {
                        signaturList.Sort((x, y) => DateTime.Compare(x.signeddate, y.signeddate));
                        lastSignaturDateTime = signaturList.Last();
                        samsvarserkleringUtf = samsvarserkleringUtforelses.First(sm => sm.signatureref.@ref == lastSignaturDateTime.id);

                        Dictionary<string, List<byggesakSignature>> signaturdictionaryType = _byggsokUtilities.GetDictionaryOfSignatureBysoknadtype(signaturList);

                        //Add history søknadstype
                        foreach (var signaturTypeSelected in signaturdictionaryType)
                        {
                            //Get signatureType to get the date and form type
                            var signaturTypes = signaturTypeSelected.Value;
                            signaturTypes.Sort((x, y) => DateTime.Compare(x.signeddate, y.signeddate));

                            //Get the last insending to get the las status (date and type)
                            lastSignaturDateTime = signaturTypes.Last();
                            var signaturTypeDateTime = lastSignaturDateTime ?? _byggsokUtilities.GetByggesakSignatureFromRefId(samsvarserkleringUtf?.signatureref.@ref, byggsok);
                            SetGjennomforingsplanDateTime(signaturTypeSelected.Key, signaturTypeDateTime?.signeddate, ansvar);
                        }
                    }
                }

                //ansvarsomraadetAvsluttet
                if (samsvarserkleringUtf != null && samsvarserkleringUtf.ansvarsrettavsluttetSpecified)
                {
                    ansvar.ansvarsomraadetAvsluttet = samsvarserkleringUtf.ansvarsrettavsluttet;
                    ansvar.erklaeringSignert = true;
                }
            }
            else
            {
                _logger.LogDebug("Unable to Get'samsvarserkleringUtforelse' ");
            }
            return ansvar;
        }

        /// <summary>
        /// Convert Byggsøk "ProAnsvarligForArbeidType" to Ftb "AnsvarsomraadeType"
        /// </summary>
        /// <param name="pro"></param>
        /// <param name="byggsok"></param>
        /// <returns></returns>
        public AnsvarsomraadeType ConvertProAnsvarligForArbeidToFtb(ProAnsvarligForArbeidType pro, byggesak byggsok)
        {
            if (Helpers.ObjectIsNullOrEmpty(pro) || Helpers.ObjectIsNullOrEmpty(byggsok))
                return null;

            AnsvarsomraadeType ansvar = null;

            ansvar = new AnsvarsomraadeType()
            {
                funksjon = new KodeType()
                {
                    kodeverdi = "PRO",
                    kodebeskrivelse = "Ansvarlig prosjektering"
                },
                ansvarsomraade = pro.beskrivelseansvarsomrade
            };

            if (pro.tiltaksklasseSpecified)
            {
                var tiltaklasseKodeverdi = (int)pro.tiltaksklasse + 1;

                ansvar.tiltaksklasse = new KodeType()
                {
                    kodeverdi = tiltaklasseKodeverdi.ToString(),
                    kodebeskrivelse = tiltaklasseKodeverdi.ToString()
                };
            }
            //legge inn partType foretak
            var ftbAnsvarligSoeker = _byggsokUtilities.GetbyggsokPartype(pro.ansvarlig?.@ref, byggsok);
            ansvar.foretak = ConvertByggsokPartTypeToForetakType(ftbAnsvarligSoeker);

            //Avkryssninger
            if (pro.ferdigatSpecified) ansvar.samsvarKontrollPlanlagtVedFerdigattest = pro.ferdigat;
            if (pro.soknadommidlerSpecified) ansvar.samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = pro.soknadommidler;
            if (pro.soknadomigangSpecified) ansvar.samsvarKontrollPlanlagtVedIgangsettingstillatelse = pro.soknadomigang;
            if (pro.soknadomrammeSpecified) ansvar.samsvarKontrollPlanlagtVedRammetillatelse = pro.soknadomramme;

            //Get all samsvarserklering/prosjektering "samsvarserkleringProsjektering"
            var proAnsvarligForArbeidId = pro.id;
            var samsvarserkleringProsjektering = _byggsokUtilities.GetSamsvarserkleringProsjektering(proAnsvarligForArbeidId, byggsok);

            if (samsvarserkleringProsjektering != null && samsvarserkleringProsjektering.Any())
            {
                //byggesakSignature lastSignaturDateTime = null;
                samsvarserkleringProsjektering samsvarserkleringPro = null;

                // select last PRO
                if (samsvarserkleringProsjektering.Length == 1)
                {
                    samsvarserkleringPro = samsvarserkleringProsjektering.FirstOrDefault();
                    var byggesakSignature = _byggsokUtilities.GetByggesakSignatureFromRefId(samsvarserkleringPro?.signatureref.@ref, byggsok);
                    if (byggesakSignature != null)
                    {
                        var lastSignaturType = _byggsokUtilities.GetSoknadTypeFrombygsokSignatur(byggesakSignature);
                        SetGjennomforingsplanDateTime(lastSignaturType, byggesakSignature?.signeddate, ansvar);
                    }
                }
                else
                {
                    List<byggesakSignature> signaturList = new List<byggesakSignature>();
                    foreach (samsvarserkleringProsjektering prosjektering in samsvarserkleringProsjektering)
                    {
                        var signature = _byggsokUtilities.GetByggesakSignatureFromRefId(prosjektering.signatureref.@ref, byggsok);
                        signaturList.Add(signature);
                    }

                    if (signaturList.Any())
                    {
                        signaturList.Sort((x, y) => DateTime.Compare(x.signeddate, y.signeddate));

                        var lastSignaturDateTime = signaturList.Last();
                        // set the first to see if the request is fulfilled
                        samsvarserkleringPro = samsvarserkleringProsjektering.First(sm => sm.signatureref.@ref == lastSignaturDateTime.id);

                        Dictionary<string, List<byggesakSignature>> signaturdictionaryType = _byggsokUtilities.GetDictionaryOfSignatureBysoknadtype(signaturList);

                        //Add history søknadstype
                        foreach (var signaturTypeSelected in signaturdictionaryType)
                        {
                            //Get signatureType to get the date and form type
                            var signaturTypes = signaturTypeSelected.Value;
                            signaturTypes.Sort((x, y) => DateTime.Compare(x.signeddate, y.signeddate));

                            //Get the last insending to get the las status (date and type)
                            lastSignaturDateTime = signaturTypes.Last();
                            var signaturTypeDateTime = lastSignaturDateTime ?? _byggsokUtilities.GetByggesakSignatureFromRefId(samsvarserkleringPro?.signatureref.@ref, byggsok);
                            SetGjennomforingsplanDateTime(signaturTypeSelected.Key, signaturTypeDateTime?.signeddate, ansvar);
                        }
                    }
                }

                //ansvarsomraadetAvsluttet
                if (samsvarserkleringPro != null && samsvarserkleringPro.ansvarsrettavsluttetSpecified)
                {
                    ansvar.ansvarsomraadetAvsluttet = samsvarserkleringPro.ansvarsrettavsluttet;
                    ansvar.erklaeringSignert = true;
                }
            }
            return ansvar;
        }

        /// <summary>
        /// Convert Byggsøk "KonAnsvarligForArbeidType" to Ftb "AnsvarsomraadeType"
        /// </summary>
        /// <param name="kontr"></param>
        /// <param name="byggsok"></param>
        /// <returns></returns>
        public AnsvarsomraadeType ConvertKonAnsvarligForArbeidToFtb(KonAnsvarligForArbeidType kontr, byggesak byggsok)
        {
            if (Helpers.ObjectIsNullOrEmpty(kontr) || Helpers.ObjectIsNullOrEmpty(byggsok))
                return null;

            AnsvarsomraadeType ansvar = null;

            //Get all samsvarserklering/utforelse "SamsvarserkleringUtforelse"
            //var konAnsvarligForArbeiId = kontr.id;
            //var samsvarserkleringKontroll = _byggsokUtilities.GetSamsvarserkleringKontroll(konAnsvarligForArbeiId, byggsok);

            ansvar = new AnsvarsomraadeType()
            {
                funksjon = new KodeType()
                {
                    kodeverdi = "KONTROLL",
                    kodebeskrivelse = "Ansvarlig kontroll"
                },
                ansvarsomraade = kontr.beskrivelseansvarsomrade
            };
            if (kontr.tiltaksklasseSpecified)
            {
                var tiltaklasseKodeverdi = (int)kontr.tiltaksklasse + 1;

                ansvar.tiltaksklasse = new KodeType()
                {
                    kodeverdi = tiltaklasseKodeverdi.ToString(),
                    kodebeskrivelse = tiltaklasseKodeverdi.ToString()
                };
            }

            //legge inn partType foretak
            var ftbAnsvarligSoeker = _byggsokUtilities.GetbyggsokPartype(kontr.ansvarlig?.@ref, byggsok);
            ansvar.foretak = ConvertByggsokPartTypeToForetakType(ftbAnsvarligSoeker);

            //Avkryssninger
            if (kontr.ferdigatSpecified) ansvar.samsvarKontrollPlanlagtVedFerdigattest = kontr.ferdigat;
            if (kontr.soknadommidlerSpecified) ansvar.samsvarKontrollPlanlagtVedMidlertidigBrukstillatelse = kontr.soknadommidler;
            if (kontr.soknadomigangSpecified) ansvar.samsvarKontrollPlanlagtVedIgangsettingstillatelse = kontr.soknadomigang;
            if (kontr.soknadomrammeSpecified) ansvar.samsvarKontrollPlanlagtVedRammetillatelse = kontr.soknadomramme;

            //Get all samsvarserklering/utforelse "kontrollerkleringKontroll"
            var proAnsvarligForArbeidId = kontr.id;
            var samsvarserkleringKontroll = _byggsokUtilities.GetSamsvarserkleringKontroll(proAnsvarligForArbeidId, byggsok);

            if (samsvarserkleringKontroll != null && samsvarserkleringKontroll.Any())
            {
                byggesakSignature lastSignaturDateTime = null;
                kontrollerkleringKontroll samsvarKont = null;

                // select last PRO
                if (samsvarserkleringKontroll.Length == 1)
                {
                    samsvarKont = samsvarserkleringKontroll.FirstOrDefault();
                    var byggesakSignature = _byggsokUtilities.GetByggesakSignatureFromRefId(samsvarKont?.signatureref.@ref, byggsok);
                    if (byggesakSignature != null)
                    {
                        var lastSignaturType = _byggsokUtilities.GetSoknadTypeFrombygsokSignatur(byggesakSignature);
                        SetGjennomforingsplanDateTime(lastSignaturType, byggesakSignature?.signeddate, ansvar);
                    }
                }
                else
                {
                    List<byggesakSignature> signaturList = new List<byggesakSignature>();
                    foreach (kontrollerkleringKontroll samsvarserkleringUtforelse in samsvarserkleringKontroll)
                    {
                        var signature = _byggsokUtilities.GetByggesakSignatureFromRefId(samsvarserkleringUtforelse.signatureref.@ref, byggsok);
                        signaturList.Add(signature);
                    }

                    if (signaturList.Any())
                    {
                        signaturList.Sort((x, y) => DateTime.Compare(x.signeddate, y.signeddate));
                        lastSignaturDateTime = signaturList.Last();
                        samsvarKont = samsvarserkleringKontroll.First(sm => sm.signatureref.@ref == lastSignaturDateTime.id);

                        Dictionary<string, List<byggesakSignature>> signaturdictionaryType = _byggsokUtilities.GetDictionaryOfSignatureBysoknadtype(signaturList);

                        //Add history søknadstype
                        foreach (var signaturTypeSelected in signaturdictionaryType)
                        {
                            //Get signatureType to get the date and form type
                            var signaturTypes = signaturTypeSelected.Value;
                            signaturTypes.Sort((x, y) => DateTime.Compare(x.signeddate, y.signeddate));

                            //Get the last insending to get the las status (date and type)
                            lastSignaturDateTime = signaturTypes.Last();
                            var signaturTypeDateTime = lastSignaturDateTime ?? _byggsokUtilities.GetByggesakSignatureFromRefId(samsvarKont?.signatureref.@ref, byggsok);
                            SetGjennomforingsplanDateTime(signaturTypeSelected.Key, signaturTypeDateTime?.signeddate, ansvar);
                        }
                    }
                }
                //ansvarsomraadetAvsluttet
                if (samsvarKont != null && samsvarKont.ansvarsrettavsluttetSpecified)
                {
                    ansvar.ansvarsomraadetAvsluttet = samsvarKont.ansvarsrettavsluttet;
                    ansvar.erklaeringSignert = true;
                }
            }

            return ansvar;
        }

        public void SetGjennomforingsplanDateTime(string soknadType, DateTime? signeddate, AnsvarsomraadeType ansvar)
        {
            if (string.IsNullOrEmpty(soknadType) || signeddate == null || Helpers.ObjectIsNullOrEmpty(ansvar))
                return;

            switch (soknadType)
            {
                case "ferdigattest":
                    ansvar.samsvarKontrollForeliggerVedFerdigattestSpecified = true;
                    ansvar.samsvarKontrollForeliggerVedFerdigattest = signeddate;
                    break;

                case "midlertidigbrukstillatelse":
                    ansvar.samsvarKontrollForeliggerVedMidlertidigBrukstillatelseSpecified = true;
                    ansvar.samsvarKontrollForeliggerVedMidlertidigBrukstillatelse = signeddate;
                    break;

                case "igangsettingstillatelse":
                case "etttrinnssoknad":
                    ansvar.samsvarKontrollForeliggerVedIgangsettingstillatelseSpecified = true;
                    ansvar.samsvarKontrollForeliggerVedIgangsettingstillatelse = signeddate;
                    break;

                case "rammetillatelse":
                    ansvar.samsvarKontrollForeliggerVedRammetillatelseSpecified = true;
                    ansvar.samsvarKontrollForeliggerVedRammetillatelse = signeddate;
                    break;

                default:
                    break;
            }
        }

        public ForetakType ConvertByggsokPartTypeToForetakType(partType partrefType)
        {
            ForetakType foretakType = null;
            //Same method to get PartType
            var ftbPartType = ConvertPartTypeFromByggsokToFtb(partrefType);
            if (ftbPartType != null)
            {
                foretakType = new ForetakType
                {
                    partstype = ftbPartType.partstype,
                    organisasjonsnummer = ftbPartType.organisasjonsnummer,
                    foedselsnummer = ftbPartType.foedselsnummer,
                    kontaktperson = ftbPartType.kontaktperson,
                    navn = ftbPartType.navn,
                    epost = ftbPartType.epost,
                    mobilnummer = ftbPartType.mobilnummer,
                    telefonnummer = ftbPartType.telefonnummer,
                    adresse = ftbPartType.adresse
                };
            }

            return foretakType;
        }

        //*****

        /// <summary>
        ///
        /// </summary>
        /// <param name="item">is byggsok "partType" but is saved as object</param>
        /// <returns></returns>
        public PartType ConvertPartTypeFromByggsokToFtb(object item)
        {
            var ftbPartType = new PartType()
            {
                partstype = new KodeType(),
                navn = "",
                adresse = new EnkelAdresseType()
                {
                    postnr = "",
                }
            };

            if (!(item is partType))
                return ftbPartType;

            var byggsokPartType = item as partType;

            var byggsokPartTypeObject = byggsokPartType.Item;
            var typeName = byggsokPartTypeObject?.GetType().Name;

            if (string.IsNullOrEmpty(typeName))
                return ftbPartType;

            switch (typeName.ToLower())
            {
                case "foretak":
                case "parttypeforetak":
                    ftbPartType = ConvertByggsokPartTypeForetakToFtb(byggsokPartTypeObject);
                    break;

                case "offentligmyndighet":
                case "parttypeoffentligmyndighet":
                    ftbPartType = ConvertbyggsokPartTypeOffentligmyndighetToFtb(byggsokPartTypeObject);
                    break;

                case "organisasjon":
                case "parttypeorganisasjon":
                    ftbPartType = ConvertByggsokPartTypeOrganisasjonToftb(byggsokPartTypeObject);
                    break;

                case "privatperson":
                case "parttypeprivatperson":
                    ftbPartType = ConvertByggsokPartTypePrivatpersonToFtb(byggsokPartTypeObject);
                    break;
            }

            return ftbPartType;
        }

        public static PartType ConvertByggsokPartTypeForetakToFtb(object item)
        {
            if (!(item is partTypeForetak partTypeForetak))
                return null;

            var partType = new PartType
            {
                partstype = new KodeType()
                {
                    kodeverdi = "foretak",
                    kodebeskrivelse = "foretak"
                },
                organisasjonsnummer = partTypeForetak.foretaksnummer,
                navn = partTypeForetak.navn,
                telefonnummer = partTypeForetak.telefonnummer,
                epost = partTypeForetak.epostadresse,
            };

            partType.adresse = ConvertByggsonEnkelAdresseToFtb(partTypeForetak.adresse);
            ConvertByggsokKontaktInformasjonToFtb(partTypeForetak.kontaktinformasjon, partType);

            return partType;
        }

        public static PartType ConvertbyggsokPartTypeOffentligmyndighetToFtb(object item)
        {
            if (!(item is partTypeOffentligmyndighet partTypeOffentligmyndighet))
                return null;

            var partType = new PartType
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Offentlig myndighet",
                    kodebeskrivelse = "Offentlig myndighet"
                },
                navn = partTypeOffentligmyndighet.navn,
            };

            partType.adresse = ConvertByggsonEnkelAdresseToFtb(partTypeOffentligmyndighet.kontaktinformasjon.adresse);
            ConvertByggsokKontaktInformasjonToFtb(partTypeOffentligmyndighet.kontaktinformasjon, partType);

            return partType;
        }

        public static PartType ConvertByggsokPartTypeOrganisasjonToftb(object item)
        {
            if (!(item is partTypeOrganisasjon partTypeOrganisasjon))
                return null;
            var partType = new PartType
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Organisasjon",
                    kodebeskrivelse = "Organisasjon"
                },
                organisasjonsnummer = partTypeOrganisasjon.organisasjonsnummer,
                navn = partTypeOrganisasjon.navn,
            };

            partType.adresse = ConvertByggsonEnkelAdresseToFtb(partTypeOrganisasjon.adresse);
            ConvertByggsokKontaktInformasjonToFtb(partTypeOrganisasjon.kontaktinformasjon, partType);

            return partType;
        }

        public static PartType ConvertByggsokPartTypePrivatpersonToFtb(object item)
        {
            if (!(item is partTypePrivatperson partTypePrivatperson))
                return null;

            var partType = new PartType
            {
                partstype = new KodeType()
                {
                    kodeverdi = "Privatperson",
                    kodebeskrivelse = "Privatperson"
                },
                navn = partTypePrivatperson.navn,
            };

            partType.adresse = ConvertByggsonEnkelAdresseToFtb(partTypePrivatperson.adresse);
            ConvertByggsokKontaktInformasjonToFtb(partTypePrivatperson.kontaktinformasjon, partType);

            return partType;
        }

        public static EnkelAdresseType ConvertByggsonEnkelAdresseToFtb(enkelAdresseType adresseType)
        {
            if (adresseType == null)
                return null;

            var ftbAdresseType = new EnkelAdresseType()
            {
                poststed = adresseType.poststed,
                postnr = adresseType.postnummer,
            };
            if (adresseType.landskodeSpecified)
                ftbAdresseType.landkode = adresseType.landskode.ToString();

            var bsAdresseType = adresseType.adresselinje?.FirstOrDefault();
            if (!string.IsNullOrEmpty(bsAdresseType?.Item?.ToString()))
                ftbAdresseType.adresselinje1 = bsAdresseType.Item?.ToString();

            return ftbAdresseType;
        }

        public static void ConvertByggsokKontaktInformasjonToFtb(kontaktinformasjonType kontaktinformasjon, PartType ftbPartType)
        {
            if (kontaktinformasjon != null)
            {
                ftbPartType.mobilnummer = ftbPartType.mobilnummer ?? kontaktinformasjon.mobiltelefonnummer;
                ftbPartType.epost = ftbPartType.epost ?? kontaktinformasjon.epostadresse;
                ftbPartType.kontaktperson = ftbPartType.kontaktperson ?? new KontaktpersonType() { navn = kontaktinformasjon.personnavn };
                ftbPartType.navn = ftbPartType.navn ?? kontaktinformasjon.personnavn;
                ftbPartType.telefonnummer = ftbPartType.telefonnummer ?? kontaktinformasjon.telefonnummer;
                if (Helpers.ObjectIsNullOrEmpty(ftbPartType.adresse))
                    ftbPartType.adresse = ConvertByggsonEnkelAdresseToFtb(kontaktinformasjon.adresse);

                // TODO What address should we use?, Should we have a logic for this?
                //partType.adresse = GetFtbEnkelAdresseType(partTypePrivatperson.kontaktinformasjon.adresse);
            }
        }
    }
}