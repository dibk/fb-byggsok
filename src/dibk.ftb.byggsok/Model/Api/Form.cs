using Newtonsoft.Json;

namespace dibk.ftb.byggsok.Model.Api
{
    /// <summary>
    /// Bruker samme modell som innsendinger i  Altinn  REST API - https://altinn.github.io/docs/api/rest/meldinger/sende-inn/#post-for-innsending-av-skjema-til-arbeidslisten-mellomlagring
    /// </summary>
    public class Form
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("dataFormatId")]
        public string DataFormatId { get; set; }
        [JsonProperty("dataFormatVersion")]
        public string DataFormatVersion { get; set; }

        /// <summary>
        /// FtB/Altinn format for skjema og underskjema data
        /// </summary>
        [JsonProperty("formData")]
        public string FormData { get; set; }
    }
}