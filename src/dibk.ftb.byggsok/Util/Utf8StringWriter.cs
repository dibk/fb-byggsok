﻿using System.IO;
using System.Text;

namespace dibk.ftb.byggsok.Util
{
    public sealed class Utf8StringWriter : StringWriter
    {
        public override Encoding Encoding
        { get { return Encoding.UTF8; } }
    }
}