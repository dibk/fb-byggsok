﻿using dibk.ftb.byggsok.Model.Api;
using dibk.ftb.byggsok.Util;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace dibk.ftb.byggsok.Services
{
    public class ByggsokToFtbService
    {
        private ILogger<ByggsokToFtbService> _logger;

        public ByggsokToFtbService(ILogger<ByggsokToFtbService> logger)
        {
            _logger = logger;
        }

        public ConvertFormTaskResponse ConvertToFTB(ByggsokRequest request)
        {
            var response = new ConvertFormTaskResponse();
            response.Type = "FormTask";
            response.EmbeddedData = new Embedded() { Forms = new List<Form>() };
            var byggsokXml = request.xml;
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokXml);

            if (byggsok == null)
                return response;

            //Skal vel ikke være mulig å gå (bakover) til ett trinn, ramme eller tiltak uten ansvar?
            if (request.nesteProsess == "IG")
            {
                response.ServiceCode = "4401";
                response.ServiceEdition = "2";
                _logger.LogDebug("Start convertering {nextProcess} ServiceCode:{serviceCode}, ServiceEdition:{serviceEdition} ", request.nesteProsess, response.ServiceCode, response.ServiceEdition);
                var formIg = new ConvertIgangsettingssoknad(_logger).Convert(byggsok);
                Form igangsettingstillatelse = new Form();
                igangsettingstillatelse.Type = "MainForm";
                igangsettingstillatelse.DataFormatId = formIg.dataFormatId;
                igangsettingstillatelse.DataFormatVersion = formIg.dataFormatVersion;
                igangsettingstillatelse.FormData = SerializeUtil.Serialize(formIg);
                response.EmbeddedData.Forms.Add(igangsettingstillatelse);
            }
            else if (request.nesteProsess == "MB")
            {
                response.ServiceCode = "4399";
                response.ServiceEdition = "5";
                _logger.LogDebug("Start convertering {nextProcess} ServiceCode:{serviceCode}, ServiceEdition:{serviceEdition} ", request.nesteProsess, response.ServiceCode, response.ServiceEdition);
                var formMb = new ConvertMidlertidigbrukstillatelse(_logger).Convert(byggsok);
                Form midlertidigbrukstillatelse = new Form();
                midlertidigbrukstillatelse.Type = "MainForm";
                midlertidigbrukstillatelse.DataFormatId = formMb.dataFormatId;
                midlertidigbrukstillatelse.DataFormatVersion = formMb.dataFormatVersion;
                midlertidigbrukstillatelse.FormData = SerializeUtil.Serialize(formMb);
                response.EmbeddedData.Forms.Add(midlertidigbrukstillatelse);
            }
            else if (request.nesteProsess == "FA")
            {
                response.ServiceCode = "4400";
                response.ServiceEdition = "2";
                _logger.LogDebug("Start convertering {nextProcess} ServiceCode:{serviceCode}, ServiceEdition:{serviceEdition} ", request.nesteProsess, response.ServiceCode, response.ServiceEdition);
                var formFa = new ConvertFerdigattest(_logger).Convert(byggsok);
                Form ferdigattest = new Form();
                ferdigattest.Type = "MainForm";
                ferdigattest.DataFormatId = formFa.dataFormatId;
                ferdigattest.DataFormatVersion = formFa.dataFormatVersion;
                ferdigattest.FormData = SerializeUtil.Serialize(formFa);
                response.EmbeddedData.Forms.Add(ferdigattest);
            }
            else if (request.nesteProsess == "ES")
            {
                response.ServiceCode = "4402";
                response.ServiceEdition = "1";
                _logger.LogDebug("Start convertering {nextProcess} ServiceCode:{serviceCode}, ServiceEdition:{serviceEdition} ", request.nesteProsess, response.ServiceCode, response.ServiceEdition);
                var formEs = new ConvertEndringssoknad(_logger).Convert(byggsok);
                Form endringssøknad = new Form();
                endringssøknad.Type = "MainForm";
                endringssøknad.DataFormatId = formEs.dataFormatId;
                endringssøknad.DataFormatVersion = formEs.dataFormatVersion;
                endringssøknad.FormData = SerializeUtil.Serialize(formEs);
                response.EmbeddedData.Forms.Add(endringssøknad);
            }

            //Uansett skal gjennomførngsplan med
            var form = new ConvertGjennomforingsplan(_logger).Convert(byggsok);
            Form gjennomføringsplan = new Form();
            gjennomføringsplan.Type = "SubForm";
            gjennomføringsplan.DataFormatId = form.dataFormatId;
            gjennomføringsplan.DataFormatVersion = form.dataFormatVersion;
            gjennomføringsplan.FormData = SerializeUtil.Serialize(form);
            response.EmbeddedData.Forms.Add(gjennomføringsplan);

            return response;
        }
    }
}