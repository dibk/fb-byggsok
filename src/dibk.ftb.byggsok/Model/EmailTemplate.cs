﻿using dibk.ftb.byggsok.Model.Api;
using dibk.ftb.byggsok.Services;

namespace dibk.ftb.byggsok.Model
{
    public class EmailTemplate
    {
        public string HtmlBody { get; set; }
        public EmailAddress From { get; set; }
        public EmailAddress To { get; set; }
        public string Subject { get; set; }

        public EmailTemplate CreateEmail(EpostRequest epostRequest)
        {
            var htmlContent = "<p>Hei!</p>\r\n<p>S&oslash;knaden fra ByggS&oslash;k er konvertert og lagret i Altinn-innboksen til ansvarlig s&oslash;ker. Referanse er&nbsp;<a href=\"{0}\">{1}</a>.</p>\r\n<p>Konverteringen gjelder: {2}.</p>\r\n<p>Du kan fortsette &aring; fylle ut og sende inn s&oslash;knaden i et av de nye s&oslash;knadssystemene. Se oversikt over godkjente s&oslash;knadsl&oslash;sninger her:&nbsp;<a title=\"Tjenestene\" href=\"http://dibk.no/tjenestene\">dibk.no/tjenestene</a></p>\r\n<p><strong>Har du sp&oslash;rsm&aring;l?</strong></p>\r\n<p>Hvis du har sp&oslash;rsm&aring;l om videre utfylling av s&oslash;knaden, ta kontakt med leverand&oslash;ren av ditt s&oslash;knadssystem.</p>\r\n<p>Hvis du har andre sp&oslash;rsm&aring;l om ByggS&oslash;k-konvertering, kan du ta kontakt med oss p&aring;&nbsp;<a href=\"mailto:ftb@dibk.no\">ftb@dibk.no</a>.</p>\r\n<p>Med vennlig hilsen<br /> Direktoratet for byggkvalitet<br /><br /></p>";
            HtmlBody = string.Format(htmlContent, epostRequest.messageUrl, epostRequest.altinArchiveReferenceNumber, epostRequest.purpose);
            From = new EmailAddress("ftb@dibk.no", "Fellestjenester BYGG");
            Subject = epostRequest.subject;
            To = new EmailAddress(epostRequest.email, epostRequest.emailSenderName);
            return this;
        }
    }
}