﻿using System.ComponentModel.DataAnnotations;

namespace dibk.ftb.byggsok.Model.Api
{
    /// <summary>
    /// Input objekt felter
    /// </summary>
    public class ByggsokRequest
    {
        /// <summary>
        /// Navn på søknadssystement som kaller APIet
        /// </summary>
        [Required]
        public string applicationSystem { get; set; }

        /// <summary>
        /// Byggsøk XML eksportert fra DiBK ByggSøk
        /// </summary>
        [Required]
        public string xml { get; set; }

        /// <summary>
        /// Type FtB datamodell/XML som blir generert fra ByggSøk XML: IG, ES, MB eller FA
        /// </summary>
        [Required]
        public string nesteProsess { get; set; }
    }
}