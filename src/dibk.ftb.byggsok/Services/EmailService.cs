﻿using dibk.ftb.byggsok.Model;
using dibk.ftb.byggsok.Model.Api;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace dibk.ftb.byggsok.Services
{
    public class EmailService
    {
        private readonly ILogger<EmailService> _logger;
        private HttpClient Client { get; }

        /// <summary>
        ///
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="configuration"></param>
        public EmailService(ILogger<EmailService> logger, HttpClient httpClient)
        {
            _logger = logger;
            Client = httpClient;
        }

        public async Task SendAsync(EpostRequest message)
        {
            try
            {
                var emailTemplate = new EmailTemplate().CreateEmail(message);

                var emailMessage = new EmailMessage
                {
                    To = new List<EmailAddress>() { emailTemplate.To },
                    HtmlBody = emailTemplate.HtmlBody,
                    Subject = emailTemplate.Subject,
                    //From = emailTemplate.From
                };

                await Post(emailMessage);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Problem creating email {email} {AltinnArkivreferanse}", message.email, message.altinArchiveReferenceNumber);
            }
        }

        private async Task Post(EmailMessage emailMessage)
        {
            try
            {
                var stringPayload = Newtonsoft.Json.JsonConvert.SerializeObject(emailMessage);
                HttpContent requestContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");

                var response = await Client.PostAsync("api/email", requestContent);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    _logger.LogInformation("Email sent to {emailaddress}", emailMessage.To.First().Address);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred when sending email to emailApi");

                throw;
            }
        }
    }

    public class EmailMessage
    {
        public IEnumerable<EmailAddress> To { get; set; }
        public IEnumerable<EmailAddress> CC { get; set; }
        public IEnumerable<EmailAddress> Bcc { get; set; }
        public EmailAddress From { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string HtmlBody { get; set; }
        public IEnumerable<EmailAttachment> Attachments { get; set; }
    }

    public class EmailAddress
    {
        [EmailAddress]
        public string Address { get; set; }

        public string DisplayName { get; set; }

        public EmailAddress(string address, string displayName)
        {
            Address = address;
            DisplayName = displayName;
        }
    }

    public class EmailAttachment
    {
        public string FileName { get; set; }
        public byte[] Content { get; set; }
        public string MimeType { get; set; }
    }
}