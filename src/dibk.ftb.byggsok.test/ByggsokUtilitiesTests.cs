﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using dibk.ftb.byggsok.Services;
using dibk.ftb.byggsok.Util;
using Xunit;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using no.dibk.byggsok;

namespace dibk.ftb.byggsok.test
{
    public class ByggsokUtilitiesTests
    {
        private readonly ILogger<SerializeUtil> _logger = new Logger<SerializeUtil>(new NullLoggerFactory());

        [Fact(DisplayName = "GetDateTimeFromSignatureRef OK")]
        public void GetDateTimeFromSignatureRefOkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var ansvarligprosjektering = byggsok.gjennomforingsplan.ansvarsfordeling.FirstOrDefault()?.ansvarligprosjektering.FirstOrDefault();

            var dateTime =new ByggsokUtilities(_logger).GetDateTimeFromSignatureRef(ansvarligprosjektering?.signatureref.@ref, byggsok);

            dateTime.Should().NotBeNull();
        }

        [Fact(DisplayName = "GetUtfAnsvarligForArbeid OK")]
        public void GetUtfAnsvarligForArbeidOkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug (4).xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);

            var utfAnsvarligForArbeidTypes = new ByggsokUtilities(_logger).GetAnsvarligForArbeidUTF(byggsok);

            utfAnsvarligForArbeidTypes.Length.Should().Be(7);
        }
        [Fact(DisplayName = "GetProAnsvarligForArbeidType OK")]
        public void GetProAnsvarligForArbeidTypeOkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);

            var proAnsvarligForArbeidTypes = new ByggsokUtilities(_logger).GetAnsvarligForArbeidPRO(byggsok);

            proAnsvarligForArbeidTypes.Should().NotBeNull();
        }
        [Fact(DisplayName = "GetKonAnsvarligForArbeidTypes OK")]
        public void GetKonAnsvarligForArbeidTypesOkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            byggesak byggesak = null;
            //byggesak byggesak = new byggesak()
            //{
            //    //gjennomforingsplan = new gjennomforingsplan()
            //    //{
            //    //    //ansvarsfordeling = new ansvarsfordeling[]
            //    //    //{
            //    //    //    //new ansvarsfordeling()
            //    //    //    //{
            //    //    //    //    //ansvarligkontroll =new KonAnsvarligForArbeidType[]
            //    //    //    //    //{
            //    //    //    //    //    //new KonAnsvarligForArbeidType()
            //    //    //    //    //    //{
            //    //    //    //    //    //    //beskrivelse = ""
            //    //    //    //    //    //}, 
            //    //    //    //    //}
            //    //    //    //}, 
            //    //    //}
            //    //}
            //};
            var proAnsvarligForArbeidTypes = new ByggsokUtilities(_logger).GetAnsvarligForArbeidKON(byggesak);

            proAnsvarligForArbeidTypes.Should().BeNull();
        }
        [Fact(DisplayName = "GetKonAnsvarligForArbeidTypes OK")]
        public void GetKonAnsvarligForArbeidTypesNotNullTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            byggesak byggesak = new byggesak()
            {
                gjennomforingsplan = new gjennomforingsplan()
                {
                    ansvarsfordeling = new ansvarsfordeling[]
                    {
                        new ansvarsfordeling()
                        {
                            ansvarligkontroll =new KonAnsvarligForArbeidType[]
                            {
                                new KonAnsvarligForArbeidType()
                                {
                                    beskrivelse = "Beskrivelse av ansvarligForArbeidType"
                                },
                            }
                        },
                    }
                }
            };
            var proAnsvarligForArbeidTypes = new ByggsokUtilities(_logger).GetAnsvarligForArbeidKON(byggesak);

            proAnsvarligForArbeidTypes[0].beskrivelse.Should().Be("Beskrivelse av ansvarligForArbeidType");
        }
        [Fact(DisplayName = "GetMunicipalityYaerAndNumber OK")]
        public void GetMunicipalityYaerAndNumberOkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_hilde.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);

            //byggesak buggsok = null;
            var buggsok = new byggesak()
            {
                saksid = new[]
                {
                    new byggesakSaksid()
                    {
                        //eier = "16/1358"
                        eier = "20161358"
                    },
                }
            };
            var kommunneInfo = new ByggsokUtilities(_logger).GetMunicipalityYaerAndNumber(buggsok);

            kommunneInfo.Count.Should().Be(2);
        }
        [Fact(DisplayName = "GetSamsvarserkleringUtforelse OK")]
        public void GetSamsvarserkleringUtforelseOkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug (4).xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);

            var samsvarserkleringUtforelse = new ByggsokUtilities(_logger).GetSamsvarserkleringUtforelse("ID_1502889166503_13893", byggsok);

            samsvarserkleringUtforelse.Length.Should().Be(1);
        }
        [Fact(DisplayName = "GetSamsvarserkleringProsjektering OK")]
        public void GetSamsvarserkleringProsjektering()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_5.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var samsvarserkleringProsjektering = new ByggsokUtilities(_logger).GetSamsvarserkleringProsjektering("ID_1484231270949_54546", byggsok);
            var samsvarserkleringProsjektering1 = new ByggsokUtilities(_logger).GetSamsvarserkleringProsjektering("ID_1455698303322_28625", byggsok);

            samsvarserkleringProsjektering.Length.Should().Be(5);
        }
        [Fact(DisplayName = "GetSamsvarserkleringKontroll OK")]
        public void GetSamsvarserkleringKontroll()
        {
            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_5.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var samsvarserkleringKontroll = new ByggsokUtilities(_logger).GetSamsvarserkleringKontroll("ID_1484231270949_68127", byggsok);
            var samsvarserkleringKontroll1 = new ByggsokUtilities(_logger).GetSamsvarserkleringKontroll("ID_1484231270949_54830", byggsok);

            samsvarserkleringKontroll.Length.Should().Be(2);
        }
        [Fact(DisplayName = "GetSoknadTypeFrombygsokSignatur OK")]
        public void GetSoknadTypeFrombygsokSignaturOkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_5.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var sig = byggsok.signature.First(s => s.id == "ID_1522231329742_1400578");
            var soknadType = new ByggsokUtilities(_logger).GetSoknadTypeFrombygsokSignatur(sig);

            soknadType.Should().Be("midlertidigbrukstillatelse");
        }
        [Fact(DisplayName = "GetSoknadTypeFrombygsokSignatur OK")]
        public void ErBerorerarbeidsplasserOkTest()
        {

            string byggsokxml = File.ReadAllText(@"Testdata\ByggesakDebug_5.xml");
            var byggsok = new SerializeUtil(_logger).DeserializeByggesakFromString<no.dibk.byggsok.byggesak>(byggsokxml);
            var berorerarbeidsplasser = new ByggsokUtilities(_logger).ErBerorerarbeidsplasser(byggsok.tiltaketsramme);

            berorerarbeidsplasser.Should().BeTrue();
        }
    }
}
